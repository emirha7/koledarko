# Koledar - spletna aplikacija

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Spletna aplikacija omogoča beleženje dogodkov in nastavljanje opomnikov ter deljenje skupnih koledarjev. 
Podprta je na večini napravah in modernih brskalnikih. Mobilne naprave, ki imajo širino manjšo od 320px žal niso podprte. 

Dostop do aplikacije: [https://obscure-depths-90855.herokuapp.com/login]
Dostop do predhodne verzije: [https://sheltered-inlet-58730.herokuapp.com/login]

![Devices.png](https://kbmerch1-a.akamaihd.net/magento/devices/images/en-us/koboaura/readAcrossDevices.png)

Aplikacija je bila testirana na:

- **brskalnikih :** Opera, Mozilla, Chrome, Edge, Safari, IE, Android brskalnik
- **mobilnih napravah :** Galaxy S5, Pixel 2, Pixel 2 XL, iPhone 5/Se, iPhone 6/7/8, iPhone 6/7/8 Plus, iPhone X
- **tablicah :** iPad, iPad Pro

# Navodila za namestitev

Klonirajte projekt v vaše c9 okolje. Ime direktorija naj bo **sp_projekt**.
Za zagon je potrebna najnovejša verzija Node.

```sh
    sudo apt-get remove mongodb-org mongodb-org-server
    sudo apt-get autoremove
    sudo rm -rf /usr/bin/mongo*
    sudo rm /etc/apt/sources.list.d/mongodb*.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
    echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list

    sudo apt-get update
    sudo apt-get install mongodb-org mongodb-org-server
    sudo touch /etc/init.d/mongod
    sudo apt-get install mongodb-org-server
    mkdir mongodb
    cd mongodb
    mkdir data
    echo 'mongod --bind_ip=$IP --dbpath=data --nojournal "$@"' > mongod
    chmod a+x mongod
    cd ~/workspace/mongodb
    ./mongod
```

Predno odprete nov terminal in poženete naslednje komande, preimenujte direktorij, ki ste ga pridobili iz GIT repozitorija v **sp_projekt**.

```sh
    cd sp_projekt
    npm install
    npm start
```


Link je oblike https://{ime_projekta_c9}-{uporabnisko_ime_c9}.c9users.io (npr. https://koledarko1123-andrej834.c9users.io).

Za dodajanje testnih podatkov je potrebno, da se najprej ustvari račun na **/register**. Nato sledi prijava v sistem, kjer kopirate **/:idKoledarja** iz spletnega naslova. Sledi navigacija na **/db**,
kamor vnesete v vnosno polje ravnokar kopiran koledar. Nato sledi pritisk na gumb **Spam database**. Dodani bodo podatki, ki jih lahko vidite preko povezave **/sharedEvents**.


# Vloge pri aplikaciji

Spletna aplikacija podpira dve vlogi:

- prijavljen uporabnik
- anonimni odjemalec

Prijavljen uporabnik ima celosten dostop do aplikacije, medtem ko anonimni odjemalec ima na voljo 
le ogled deljenih koledarjev. V primeru dostopa do strani, ki mu niso na voljo, ga preusmeri
na **/login**.

# Analiza časa nalaganja strani na brskalniku Chrome in Opera

Ob prvem zahtevku aplikacija potrebuje največ časa da se naloži. Vsi nadaljni zahtevki, pa so potrebni, za dinamično dodajanje pogledov in statičnih datotek. Spodnja tabela
prikazuje rezultate testiranja.

| Routes                                | Chrome  | Opera   |
| --------------------------------------|---------|---------|
| app                                   | 8.52 s  | 8.31 s  |
| login                                 | 662  ms |  664 ms |
| register                              | 697  ms |  685 ms |
| db                                    | 914  ms | 1271 ms |
| calendar/:idKoledarja                 | 1070 ms | 1060 ms |
| calendar/:idKoledarja/event           | 2050 ms | 2656 ms |
| calendar/:idKoledarja/event/add       | 1320 ms | 1039 ms |
| calendar/:idKoledarja/event/:idEventa | 1115 ms | 1404 ms |
| sharedEvents                          | 2444 ms | 2642 ms |
| sharedEvents/:idEventa                | 1298 ms | 1298 ms |
| search                                | 732  ms |  762 ms |
| searchResults                         | 1389 ms |  466 ms |
| sharedCalendar                        | 1328 ms | 1794 ms |
| sharedCalendar/:idKoledarja           | 1646 ms |  910 ms |
| sharedCalendar/:idKoledarja/event     | 1480 ms |  646 ms |
| account/:idAccounta                   | 1869 ms |  710 ms |


Opazimo, da največ časa potrebuje pot **/sharedEvents**, saj ima ta veliko število deljenih dogodkov (v temu koraku bi bila potrebna paginacija, da bi omejila, koliko podatkov se pošlje ob enem zahtevku).
Z veliko količino podatkov, pride tudi do množičnega dodajanja HTML vozlišč v sam dokument, kar pa ni ravno najhitrejša operacija. Poleg predhodno omenjenih težav, je potrebno še omeniti Google Analytics,
ki tudi potrebuje nekaj časa, da v ozadju izvede, kar je njegov namen. Tukaj se vidi, da so časi posameznih nalaganj odvisni predvsem od števila elementov v bazi. 

Predhodno je največ težav povzročal **/register**, saj je ta nalagal 3MB veliko sliko za ozadje HTML dokumenta. 
To se je dalo enostavno rešiti z kompresiranjem. Zgornji časi so rezultat IIFE pristopa, kompresiranja slik, kode AngularJS in CSS datotek.

# JMeter analiza spletne aplikacije

Apache JMeter je orodje, ki nam omogoča testiranje zmogljivosti aplikacij. Ena izmed njegovih ključnih funkcionalnosti je testiranje večkratnega števila uporabnikov.

Sama obremenitev strežnika je odvisna predvsem od strojne opreme. Spodnja slika predstavlja le del rezultatov, pri čemer opazimo, da je sistem v določenem trenutku popustil, saj ni bil zmožen 
tolikšnega števila uporabnikov hkrati. Sama AngularJS aplikacija se veliko bolje odnese kot njena predhodna verzija, ki je bila narejena s pomočjo Express strežnika in PUG predlog. Dosegli smo skoraj 10x
izboljšavo.

![JMeter.png](https://dl.dropbox.com/s/5fsee3kx185zlvt/image.png)

Specifikacije sistema: 

- Operacijski sistem: Windows 10 Pro 64-bit
- CPE: Intel Core 2 Quad Q9400 @ 2.66Ghz
- RAM: 8.00GB Dual-Channel DDR3 
- Shramba: SSD 750 Evo 250GB
- Omrežna statistika: 146.34/3.92 Mpbs, ping: 6 ms

Strežnik je bil pognan lokalno (ločen dostop do MLAB baze kot tudi heroku aplikacije). Po predhodni specifikaciji je razvidno, da strojna oprema ni namenjena vzdrževanju strežnika. Boljše rezultate bi dobili,
če bi strežnik nadgradili.

# Lokalno testiranje na računalniku izven Cloud9 okolja

Sam projekt se prenese na računalnik v poljubno izbran direktorij. Od tam odprete **Powershell**, v katerem poženete sledeče ukaze:

- npm install
- $env:NODE_ENV = 'production';
- $env:MLAB_URI = 'mongodb://admin1:admin1@ds227654.mlab.com:27654/koledarkoapp'
- npm start

Tako se sedaj lahko požene ZAP test.

# Funkcionalnosti

Aplikacija vam omogoča:

- imeti svoj koledar
- beleženje posameznih dogodkov v koledar
- skupinski koledarji, ki so na voljo vsem
- deljenje posameznih dogodkov
- iskanje po dogodkih

Aplikacija je trenutno sestavljena iz več zaslonskih mask:

- [registracija uporabnika]
- [prijava]
- [pregled svojega koledarja]
- [pregled vseh dogodkov za določen dan]
- [urejanje koledarja]
- [pregled skupinskih koledarjev]
- [dodajanje dogodka v koledar]
- [urejanje dogodkov]
- [brisanje dogodkov iz koledarja]
- [uporabniške nastavitve]
- [pregled deljenih dogodkov]
- [iskanje po dogodkih]

# Opis posameznih mask
### Registracija uporabnika
Predno lahko posameznik uporablja spletno aplikacijo se more registrirati. Ob tem je potrebno vpisati naslednje podatke:

- uporabniško ime, ki more biti daljše od 6 znakov ter krajše od 20 znakov
- email na katerega se bo pošiljalo obvestila
- geslo, ki mora biti daljše od 6 znakov in krajše od 20 znakov
- ponovitev gesla, ki more ustrezati predhodnemu vnosu v polje geslo

Vsa polja so obvezna. V primeru, da je katero izmed polj neizpolnjeno ali pa je neustrezno se prikaže uporabniku obvestilo.

### Prijava
Če ima uporabnik že kreiran račun ali pa ga je ravnokar kreiral se lahko vpiše v spletno aplikacijo. Pri tem mora vpisati sledeče podatke:

- uporabniško ime in geslo

Če je prijava uspešna se uporabnika preusmeri na začetno spletno stran (calendar.html).

### Pregled svojega koledarja
Vsak registriran uporabnik ima svoj koledar, v katerega lahko shranjuje dogodke. Na zaslonu se uporabnik lahko premika med posameznimi mesci ter znotraj vsakega izbira dan, za katerega si želi ogledati svoje dogodke ali pa želi dodati nove. Vsak lahko deli svoj koledar z drugimi. Stran je opremljena tudi z menijem, ki omogoča navigacijo med deljenimi koledarji, deljenimi dogodki, iskanjem po dogodkih, nastavitve računa in odjavo. Meni je prisoten na vseh zaslonskih maskah razen pri prijavi in registraciji.
    
### Pregled vseh dogodkov za določen dan 
Za določen dan si lahko uporabnik ogleda že dodane dogodke, jih posodobi ali pa doda nove. Pri tem ga preusmerimo na ustrezne zaslonske maske glede na zahtevo. Ob kliku na gumb + se uporabnika preusmeri na stran dodajanje dogodka. Za urejanje dogodka mora uporabnik klikniti na gumb zobnik.

Zaslonska maska je opremljena z datumom, ki ustreza dnevu, katerega uporabnik želi spremeniti. Prisotna je tudi vremenska napoved, ki napove kakšno vreme bo bilo tisti dan.
Informacije o vremenu so pridobljene s pomočjo zunanjega vira(API - https://www.apixu.com/api.aspx).
    
### Urejanje koledarja    

Koledar lahko tudi uredimo. Na voljo imamo spremembo naslova ter opisa. Uporabnik se nato lahko odloči ali bo koledar delil z drugimi. To stori z klikom na gumb share. Vsebina vnosnih polj je obvezna in je poljubna.
    
### Pregled skupinskih koledarjev    
    
Uporabnik lahko vidi vse koledarje, ki so bili deljeni z javnostjo. Ob kliku na posamezen koledar se uporabnika preusmeri na pregled koledarja.
    
### Dodajanje dogodka v koledar
Posamezen dogodek je sestavljen iz sledečih elementov:

- naslov
- lokacija
- čas začetka
- čas konca
- opis dogodka

Za dodajanje dogodka je potrebno izpolniti vsa polja. Dodajanje potrdimo s klikom na gumb Confrim. Vnos v polja je obvezen. Čas začetka in čas konca sprejmeta čas, medtem ko ostala vnosna polja sprejmejo poljuben vnos.
    
### Urejanje dogodka

Uporabnik lahko posamezen podatek o dogodku spremeni s klikom na pisalo v pripadajoči vrstici. Pri tem morajo biti izpolnjena vsa polja. 

Dogodek, ki ga urejamo, lahko delimo z javnostjo. To storimo s klikom na gumb Share, ki nas preusmeri na deljenje dogodke. Za odstranitev dogodka iz koledarja, je potreben klik na gumb Remove. Ta nas preusmeri na zaslonsko masko odstrani dogodek.

Vnosna polja imajo enako strukturo, kot je bilo že predhodnje opisano v razdelku **Dodajanje dogodka v koledar**.
    
### Brisanje dogodkov iz koledarja

Stran nas opozori o naši odločitvi za izbris dogodka ter izpiše njegov čas in naziv. Izbiro potrdimo s klikom na gumb Confrim, ki nas nato preusmeri na pregled dogodkov za isti dan.

### Pregled deljenih dogodkov

Stran prikazuje dogodke, ki so bili deljeni z javnostjo. Vsak dogodek je opremljen z naslovom, datumom, uro, ikono in kratkim opisom. Ob kliku na posamezen deljen dogodek se uporabniku prikažejo podrobnosti o njem.

### Uporabniške nastavitve

Stran omogoča spremembo vzdevka kot tudi gesla. Pri tem zahtevamo staro geslo in dvakratni vnos novega.

Za shranitev novo izbranih nastavitev je potrebno klikniti na gumb Submit. Ob tem se preveri ustreznost vnosnih polj.
Vnosna polja so sledeča:

- vzdevek (ob registraciji je enak uporabniškemu imenu), lahko zavzema največ 20 znakov
- staro geslo (ustreza geslu, ki ga ima trenutno prijavljen uporabnik), zavzema najmanj 6 do največ 20 znakov
- novo geslo (novo geslo, ki ga uporabnik želi imeti), zavzema najmanj 6 do največ 20 znakov
- potrdi novo geslo (vnosno polje se mora ujemati z novim geslom)

Vsa vnosna polja morajo biti izpolnjena. Njihova vsebina je poljubna.

### Iskanje po dogodkih
Uporabnik lahko išče po deljenih dogodkih. Na voljo je le en atribut, ki je obvezen:

- lokacija

Vnos je poljuben. V primeru, da noben dogodek ne obstaja, se uporabniku izpiše na zaslon opozorilo.

   [https://obscure-depths-90855.herokuapp.com/login]: <https://obscure-depths-90855.herokuapp.com/login>
   [https://sheltered-inlet-58730.herokuapp.com/login]: <https://sheltered-inlet-58730.herokuapp.com/login>
   [registracija uporabnika]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/signup.html>
   [prijava]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/login.html>
   [pregled svojega koledarja]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/calendar.html>
   [pregled vseh dogodkov za določen dan]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/dayPreview.html>
   [urejanje dogodkov]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/editEvent.html>
   [urejanje koledarja]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/editCalendar.html>
   [pregled skupinskih koledarjev]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/sharedCalendars.html>
   [dodajanje dogodka v koledar]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/addEvent.html>
   [pregled deljenih dogodkov]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/sharedEvents.html>
   [iskanje po dogodkih]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/search.html>
   [brisanje dogodkov iz koledarja]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/removeEvent.html>
   [uporabniške nastavitve]: <https://bitbucket.org/aljazmark/sp_projekt/src/master/docs/profileSettings.html>