package automationFramework;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import exceptions.AddEventException;
import exceptions.LoginFailException;
import exceptions.NumberOfElementsMismatchException;
import exceptions.RedirectException;

public class TestCase {

	public static void main(String[] args) {

		String currentDir = System.getProperty("user.dir");
		System.setProperty("webdriver.gecko.driver",
				currentDir + "\\geckodriver.exe");	
		
		WebDriver driver = new FirefoxDriver();
		String url = "https://obscure-depths-90855.herokuapp.com/login";

		driver.get(url);

		delay(4000);

		System.out.println("Connection [OK] => " + url);

		// trying to login
		doLogin("selenium", "selenium", driver);

		// trying to search for a shared event
		doSearch(driver, "Siska123", 1);

		// trying to add an event
		doAddEvent(driver);

		System.out.println("Finished [OK] => " + driver.getCurrentUrl());

		driver.quit();

	}

	public static void delay(int delay) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void doLogin(String name, String pass, WebDriver driver) {
		WebElement userElement = driver.findElement(By.id("uname"));
		userElement.sendKeys(name);

		WebElement passElement = driver.findElement(By.id("Password"));
		passElement.sendKeys(pass);

		WebElement login = driver.findElement(By.className("btn-login"));
		login.click();

		delay(1500);

		String currentUrl = driver.getCurrentUrl();

		try {
			if (currentUrl.contains("obscure-depths-90855.herokuapp.com/calendar/")) {
				System.out.println("Login [OK] => " + currentUrl);
			} else {
				throw new LoginFailException();
			}
		} catch (LoginFailException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void doSearch(WebDriver driver, String location, int expectedSearchSize) {
		WebElement searchRedirect = driver.findElement(By.id("searchRedirect"));

		searchRedirect.click();

		delay(1000);

		String currentUrl = driver.getCurrentUrl();

		try {
			if (currentUrl.contains("obscure-depths-90855.herokuapp.com/search")) {
				System.out.println("Redirect to Search [OK] => " + currentUrl);
			} else {
				throw new RedirectException(currentUrl);
			}
		} catch (RedirectException e) {
			e.printStackTrace();
			System.exit(1);
		}

		WebElement searchField = driver.findElement(By.className("input-b"));
		searchField.clear();
		delay(300);

		searchField.sendKeys(location);
		delay(200);

		driver.findElement(By.className("save-changes")).click();

		delay(2000);

		currentUrl = driver.getCurrentUrl();

		System.out.println(currentUrl);
		
		try {
			if (currentUrl.contains("obscure-depths-90855.herokuapp.com/search")
					&& currentUrl.contains("?p=0&location=" + location)) {
				System.out.println("Search [OK] => " + currentUrl);
			} else {
				throw new RedirectException(currentUrl);
			}
		} catch (RedirectException e) {
			e.printStackTrace();
			System.exit(1);
		}

		int returnResult = driver.findElements(By.className("details")).size();

		try {
			if (expectedSearchSize == returnResult) {
				System.out.println("Number of elements [OK] => " + currentUrl);
			} else {
				throw new NumberOfElementsMismatchException(expectedSearchSize, returnResult);
			}
		} catch (NumberOfElementsMismatchException e) {
			e.printStackTrace();
			System.exit(1);
		}

		delay(2000);
	}

	public static void doAddEvent(WebDriver driver) {
		WebElement calendarRedirect = driver.findElement(By.id("calendarRedirect"));

		calendarRedirect.click();

		delay(1000);

		String currentUrl = driver.getCurrentUrl();

		try {
			if (currentUrl.contains("obscure-depths-90855.herokuapp.com/calendar/")) {
				System.out.println("Redirect [OK] => " + currentUrl);
			} else {
				throw new RedirectException(currentUrl);
			}
		} catch (RedirectException e) {
			e.printStackTrace();
			System.exit(1);
		}

		WebElement someDate = driver.findElement(By.xpath("//ul[@class='row']/li[10]/div/a"));
		someDate.click();
		delay(2000);
		
		currentUrl = driver.getCurrentUrl();


		System.out.println("Redirect [OK] => " + currentUrl);

		String returnUrl = driver.getCurrentUrl();
		WebElement addEventButton = driver.findElement(By.className("add-event"));
		addEventButton.click();
		delay(2000);
		currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		try {
			if (currentUrl.contains("obscure-depths-90855.herokuapp.com/calendar/") && currentUrl.contains("/add")) {
				System.out.println("Redirect [OK] => " + currentUrl);
			} else {
				throw new RedirectException(currentUrl);
			}
		} catch (RedirectException e) {
			e.printStackTrace();
			System.exit(1);
		}

		WebElement titleField = driver.findElement(By.xpath("//input[@name = 'title']"));
		WebElement locationField = driver.findElement(By.xpath("//input[@name = 'location']"));
		WebElement startTimeField = driver.findElement(By.xpath("//input[@name = 'startTime']"));
		WebElement endTimeField = driver.findElement(By.xpath("//input[@name = 'endTime']"));
		WebElement noteField = driver.findElement(By.xpath("//input[@name = 'note']"));

		titleField.sendKeys("Selenium Test 123");
		delay(1000);
		locationField.sendKeys("SiskaNice");
		delay(1000);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		startTimeField.sendKeys("10:12");
		//jse.executeScript("arguments[0].value='16:00';", startTimeField);
		delay(1000);
		endTimeField.sendKeys("14:12");
		//jse.executeScript("arguments[0].value='18:00';", endTimeField);
		delay(1000);
		noteField.sendKeys("Best note ever!");

		delay(1000);

		driver.findElement(By.className("add-event")).click();

		delay(2000);

		currentUrl = driver.getCurrentUrl();
		
		WebElement adding = driver.findElement(By.xpath("//div[@class='alert alert-success']/strong"));
		System.out.println(adding.getText());
		if(adding.getText().equals("The data was added")) {
			System.out.println("Adding event [OK] => " + currentUrl);
		}
		
		System.out.println("Adding event [NOK] => " + currentUrl);
	
	}
}
