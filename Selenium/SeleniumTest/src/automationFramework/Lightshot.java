package automationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Lightshot {
	public static void main(String[] args) throws InterruptedException {
		String currentDir = System.getProperty("user.dir");
		System.setProperty("webdriver.gecko.driver",
				currentDir + "\\geckodriver.exe");	
		
		WebDriver driver = new FirefoxDriver();
		String urlStart = "https://prnt.sc/m55c";
		char[] alphabet = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
		
		for(int i = 0; i < alphabet.length; i++) {
			String newUrl = urlStart + alphabet[i];
			for (int j = 0; j < alphabet.length; j++) {
				driver.get(newUrl+alphabet[j]);
				Thread.sleep(100);
			}
		}
		
		
	}
}
