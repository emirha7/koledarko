package exceptions;

public class LoginFailException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public LoginFailException() {
		super("Login [NOK] => Quitting application.");
	}
}
