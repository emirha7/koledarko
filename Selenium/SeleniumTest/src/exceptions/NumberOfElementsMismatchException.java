package exceptions;

public class NumberOfElementsMismatchException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public NumberOfElementsMismatchException(int expected, int got) {
		super("Number of elements [NOK] => expected: " + expected + ", got: " + got);
	}
	
}
