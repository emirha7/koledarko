package exceptions;

public class RedirectException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public RedirectException(String m) {
		super("Redirect [NOK] => " + m);
	}
}
