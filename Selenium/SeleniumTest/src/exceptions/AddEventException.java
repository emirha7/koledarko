package exceptions;

public class AddEventException extends Exception {
	private static final long serialVersionUID = 1L;

	public AddEventException() {
		super("Adding event [NOK] => Quitting application.");
	}
}
