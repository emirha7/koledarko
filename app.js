var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var uglifyJs = require('uglify-js');
var fs = require('fs');
var passport = require('passport');

var xss = require("xss");
var bodyParser = require('body-parser');
var expressSanitized = require('express-sanitize-escape');

require('dotenv').load();
require('./app_api/models/db');
require('./app_api/konfiguracija/passport');

process.env.NODE_ENV = 'production';

var zdruzeno = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
  'login.krmilnik.js': fs.readFileSync('app_client/auth/login.krmilnik.js', 'utf-8'),
  'login.storitev.js': fs.readFileSync('app_client/auth/login.storitev.js', 'utf-8'),
  'register.krmilnik.js': fs.readFileSync('app_client/auth/register.krmilnik.js', 'utf-8'),
  'register.storitev.js': fs.readFileSync('app_client/auth/register.storitev.js', 'utf-8'),
  'navigation.directive.js': fs.readFileSync('app_client/skupno/directive/navigacija/navigation.directive.js', 'utf-8'),
  'calendar.krmilnik.js': fs.readFileSync('app_client/calendar/main/calendar.krmilnik.js', 'utf-8'),
  'calendar.storitev.js': fs.readFileSync('app_client/skupno/storitve/calendar.storitev.js', 'utf-8'),
  'calendarEditModalnoOkno.krmilnik.js': fs.readFileSync('app_client/modalnoOkno/calendarEditModalnoOkno.krmilnik.js', 'utf-8'),
  'user.krmilnik.js': fs.readFileSync('app_client/calendar/user/user.krmilnik.js', 'utf-8'),
  'getUserInfo.storitev.js': fs.readFileSync('app_client/calendar/user/getUserInfo.storitev.js', 'utf-8'),
  'sendUserInfo.storitev.js': fs.readFileSync('app_client/calendar/user/sendUserInfo.storitev.js', 'utf-8'),
  'saveCalendar.storitev.js': fs.readFileSync('app_client/calendar/skupneStoritve/saveCalendar.storitev.js', 'utf-8'),
  'sharedCalendar.krmilnik.js': fs.readFileSync('app_client/calendar/shared/sharedCalendar.krmilnik.js', 'utf-8'),
  'sharedEvents.krmilnik.js': fs.readFileSync('app_client/calendar/sharedEvents/sharedEvents.krmilnik.js', 'utf-8'),
  'sharedCalendar.storitev.js': fs.readFileSync('app_client/calendar/shared/sharedCalendar.storitev.js', 'utf-8'),
  'sharedEvents.storitev.js': fs.readFileSync('app_client/calendar/sharedEvents/sharedEvents.storitev.js', 'utf-8'),
  'dayPreview.krmilnik.js': fs.readFileSync('app_client/calendar/dayPreview/dayPreview.krmilnik.js', 'utf-8'),
  'geolocation.storitev.js': fs.readFileSync('app_client/skupno/storitve/geolocation.storitev.js', 'utf-8'),
  'weatherService.storitev.js': fs.readFileSync('app_client/skupno/storitve/weatherService.storitev.js', 'utf-8'),
  'eventAdd.krmilnik.js': fs.readFileSync('app_client/calendar/event/eventAdd.krmilnik.js', 'utf-8'),
  'sendEvent.storitev.js': fs.readFileSync('app_client/skupno/storitve/sendEvent.storitev.js', 'utf-8'),
  'eventEdit.krmilnik.js': fs.readFileSync('app_client/calendar/event/eventEdit.krmilnik.js', 'utf-8'),
  'event.storitev.js': fs.readFileSync('app_client/skupno/storitve/event.storitev.js', 'utf-8'),
  'updateEvent.storitev.js': fs.readFileSync('app_client/skupno/storitve/updateEvent.storitev.js', 'utf-8'),
  'deleteEvent.storitev.js': fs.readFileSync('app_client/skupno/storitve/deleteEvent.storitev.js', 'utf-8'),
  'showSharedCalendar.krmilnik.js': fs.readFileSync('app_client/calendar/shared/showSharedCalendar.krmilnik.js', 'utf-8'),
  'sharedCalendarPreview.krmilnik.js': fs.readFileSync('app_client/calendar/shared/sharedCalendarPreview.krmilnik.js', 'utf-8'),
  'search.krmilnik.js': fs.readFileSync('app_client/search/search.krmilnik.js', 'utf-8'),
  'search.storitev.js': fs.readFileSync('app_client/search/search.storitev.js', 'utf-8'),
  'searchResults.krmilnik.js': fs.readFileSync('app_client/search/searchResults.krmilnik.js', 'utf-8'),
  'sharedEventsPreview.krmilnik.js': fs.readFileSync('app_client/calendar/sharedEvents/sharedEventsPreview.krmilnik.js', 'utf-8'),
  'logout.krmilnik.js': fs.readFileSync('app_client/logout/logout.krmilnik.js', 'utf-8'),
  'db.krmilnik.js': fs.readFileSync('app_client/db/db.krmilnik.js', 'utf-8'),
  'db.storitev.js': fs.readFileSync('app_client/db/db.storitev.js', 'utf-8'),
  'avtentikacija.storitev.js': fs.readFileSync('app_client/auth/avtentikacija.storitev.js', 'utf-8')
});

fs.writeFile('public/angular/koledarko.min.js', zdruzeno.code, function(napaka) {
  if (napaka)
    console.log(napaka);
  else
    console.log('Skripta je zgenerirana in shranjena v "koledarko.min.js".');
});

var indexApi = require('./app_api/routes/index');
var angularApp = require("./app_server/routes/angular");

var app = express();

app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSanitized.middleware());
app.use(function(req, res, next){
	for(var key in req.body){
		if(Array.isArray(req.body[key]) || typeof req.body[key] === 'object'){
			continue;
		}
		req.body[key] = xss(req.body[key]);
	}
	next();
});


// view engine setup
app.set('views', path.join(__dirname, 'app_server' ,'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());
app.use('/api', indexApi);
app.use("/", angularApp);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "sporočilo": err.name + ": " + err.message
    });
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
