var uidG;
var cidG;

window.onload = function(e){ 	
	var uid = document.getElementById("userId");
	var cid = document.getElementById("calendarId");
	
	if(uid.innerHTML != ""){
		uidG = uid.innerHTML;
	    console.log(uidG);
	}else{
		location.href = "/";
	}
	
	if(cid.innerHTML != ""){
		cidG = cid.innerHTML;
		console.log(cidG);
	}else{
		location.href = "/";
	}
    
	setCookie("uid", uidG);
	document.getElementById("accountRedirect").href += uidG;
	
	setCookie("cid", cidG);
	document.getElementById("calendarRedirect").href += cidG;
	document.getElementById("searchRedirect").href += cidG;
	
	uid.remove();
	cid.remove();
}

function setCookie(cname, cvalue) {
  var d = new Date();
  d.setTime(d.getTime() + (60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}