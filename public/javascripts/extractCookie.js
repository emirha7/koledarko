window.onload = function(e){ 	

    var uid = getCookie("uid");
    var cid = getCookie("cid");

    if(uid != ""){
        document.getElementById("accountRedirect").href += uid;
    }else{
        location.href = "/";
    }
    
    if(cid != ""){
        document.getElementById("calendarRedirect").href += cid;
        document.getElementById("searchRedirect").href += cid;
    }else{
        location.href = "/";
    }
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}