function getPreviousMonthURL(){
	var prev = new Date(extractQuery());
	prev.setDate(1);
	prev.setMonth(prev.getMonth()-1);
	var queryString = prev.getFullYear() + "-" + (prev.getMonth() + 1) + "-" + prev.getDate();
	var redirectUrl = location.pathname + "?q=" + queryString;
	location.href = redirectUrl;
}

function getNextMonthURL(){
	var next = new Date(extractQuery());
	next.setDate(1);
	next.setMonth(next.getMonth()+1);
	var queryString = next.getFullYear() + "-" + (next.getMonth() + 1) + "-" + next.getDate();
	var redirectUrl = location.pathname + "?q=" + queryString;
	location.href = redirectUrl;
}

function extractQuery(){
	var q = location.search.split("?q=");
	if(q.length == 2)
		return q[1];
	
	return new Date();
}