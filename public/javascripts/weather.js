function getLocation() {
  console.log("get location!!!!!!!!");
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(getWeather);
  } else {
   alert("GEOLOCATION NOT SUPPORTED BY THIS BROWSER");
  }
}

function getWeather(position){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
      document.querySelector(".img-circle").src = "https:"+this.responseText.substring(1,this.responseText.length-1);
    }
  };
  
  console.log(window.location.host);
  var d = parseDate();
  var strDate = "&year="+d.year+"&month="+d.month+"&day="+d.day;
  
  xhttp.open("GET", "https://"+window.location.host+"/weather?lat="+position.coords.latitude+"&long="+position.coords.longitude+strDate, true);
  xhttp.send();
}


function parseDate(){
  var ym = document.querySelector(".h1-month").innerText.split(" ");
  var mesci = ['January','February','March','April','May','June','July','August','September','October','November','December'];
  return {
    year: ym[1],
    month: mesci.indexOf(ym[0])+1,
    day : document.querySelector(".h1-day").innerText.split(" ")[1]
  }
}

getLocation(); 
