(function() {
  function logoutCtrl($rootScope, $location, avtentikacija) {
     var vm = this;
  
	 
	 var redirectLog = function(){
		$location.path("/login");
		console.log("logged out");
	 }
	 
     
  
     $rootScope.css = "";
     $rootScope.calendar = "";
     $rootScope.user = "";
	 avtentikacija.odjava(redirectLog);
    
  }
  
  logoutCtrl.$inject = ["$rootScope", "$location", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("logoutCtrl", logoutCtrl);
})();