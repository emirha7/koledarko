(function(){
    var saveCalendar = function($http, avtentikacija){
        var saveCal = function(idCalendar, podatki){
            return $http.put("api/calendars/"+idCalendar, podatki, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            saveCal: saveCal      
        };
    };
    
    saveCalendar.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("saveCalendar", saveCalendar);
})();