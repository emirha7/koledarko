(function() {
  function eventAddCtrl($rootScope, $location, geolocationService, weatherService, sendEventData, avtentikacija) {
    
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }
    
    var vm = this;
    $rootScope.css = "/css/addEvent.css";
    
    var appendingDataDynamicly = function(){
		var currentDate = new Date($location.search().q);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();
		
		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		vm.calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};
    };
    
    appendingDataDynamicly();
    
    vm.cbError = function(){
        $rootScope.apply(function(){
           alert("PRISLO JE DO NAPAKE?"); 
        });
    };
    
    vm.cbNotSupported = function(){
        $rootScope.apply(function(){
           alert("browser not supported"); 
        });
    };
    
    vm.vreme = "";
    
    vm.callback = function(lokacija){
        var lat = lokacija.coords.latitude;
        var lng = lokacija.coords.longitude;
        
        var currentDate = new Date($location.search().q);
    	var currentDay = currentDate.getDate();
    	var currentMonth = currentDate.getMonth()+1;
    	var currentYear = currentDate.getFullYear();
        
   		if (currentMonth < 10)
			currentMonth = "0" + currentMonth;

		if (currentDay < 10)
			currentDay = "0" + currentDay;
        
        weatherService.getWeather(lat, lng, "1a2ac4e5ca3c476494c212254180812",currentYear+"-"+currentMonth+"-"+currentDay).then(
            function success(odgovor){
                vm.slika = "https:"+odgovor.data.current.condition.icon;
                vm.vreme = "true";
            }, function napaka(odgovor){
				//napaka
            });
    };
    
    vm.posljiPodatke = function(){
        if(!vm.podatkiObrazca.title || !vm.podatkiObrazca.location || !vm.podatkiObrazca.startTime || !vm.podatkiObrazca.endTime || !vm.podatkiObrazca.note){
            vm.napakaSporocilo = "Missing fields";
        }else{
            function concatTimeAndDate(time, date) {
            	var d = new Date(date);
            	var minute = time.getMinutes();
            	var ure = time.getHours();
            
            	d.setHours(ure);
            	d.setMinutes(minute);
            
            	return d;
            }
            
        	var startTime = concatTimeAndDate(vm.podatkiObrazca.startTime, $location.search().q);
        	var endTime = concatTimeAndDate(vm.podatkiObrazca.endTime, $location.search().q);
        	
    		var json = {
        		title: vm.podatkiObrazca.title,
        		location: vm.podatkiObrazca.location,
        		startTime: startTime,
        		endTime: endTime,
        		note: vm.podatkiObrazca.note,
        		shared: "false"
        	};
        	
        	
        	vm.uspeh = "";
        	sendEventData.posljiData($rootScope.calendar, json).then(
        	    function success(odgovor){
        	        vm.napakaSporocilo = "";
        	        vm.uspeh = "true";
        	    },
        	    function error(odgovor){
        	        vm.napakaSporocilo = "There was an error";
        	    });
        }
    };
    
    geolocationService.getCurrentLocation(vm.callback, vm.cbError, vm.cbNotSupported);
  }
  
  eventAddCtrl.$inject = ["$rootScope", "$location", "geolocationService", "weatherService", "sendEventData", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("eventAddCtrl", eventAddCtrl);
})();