(function(){
    var sharedEventsStoritev = function($http){
        var showAllShared = function(){
            return $http.get("api/events/shared");
        };
        
        return {
            showAllShared: showAllShared      
        };
    };
    
    sharedEventsStoritev.$inject = ["$http"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("sharedEventsStoritev", sharedEventsStoritev);
})();