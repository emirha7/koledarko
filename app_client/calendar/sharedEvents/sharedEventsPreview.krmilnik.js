(function() {
  function sharedEventsPreviewCtrl($rootScope, $location, $routeParams, eventDetail, avtentikacija) {
    
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }  
    
    var vm = this;
    $rootScope.css = "/css/editEvent.css";
    
    vm.podatkiObrazca = {};
    
    var appendingDataDynamicly = function(date){
		var currentDate = new Date(date);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();
		
		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		vm.calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};
    };
    
    eventDetail.getDetails($routeParams.idEventa).then(
        function success(odgovor){

            vm.podatkiObrazca.title = odgovor.data.title;
            vm.podatkiObrazca.location = odgovor.data.location;
            vm.podatkiObrazca.note = odgovor.data.note;
            vm.podatkiObrazca.shared = odgovor.data.shared;
            vm.podatkiObrazca.startTime = new Date(odgovor.data.startTime);
            vm.podatkiObrazca.endTime = new Date(odgovor.data.endTime);
            
            appendingDataDynamicly(odgovor.data.startTime);
            
        },
        function error(odgovor){
           //napaka
        });
    
  }
  
  sharedEventsPreviewCtrl.$inject = ["$rootScope", "$location", "$routeParams","eventDetail", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("sharedEventsPreviewCtrl", sharedEventsPreviewCtrl);
})();