(function() {
  function sharedEventsCtrl($rootScope, $location, sharedEventsStoritev, avtentikacija) {
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }  
      
    var vm = this;
    $rootScope.css = "/css/sharedCalendars.css";
    
    sharedEventsStoritev.showAllShared().then(function success(odgovor){
        if(odgovor.data.length != 0){
            vm.events = odgovor.data;   
        }else{
            vm.prikaziPrazno = "yes";
        }
    },
    function error(odgovor){
        //napaka
    });
  }
  
  sharedEventsCtrl.$inject = ["$rootScope", "$location", "sharedEventsStoritev", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("sharedEventsCtrl", sharedEventsCtrl);
})();