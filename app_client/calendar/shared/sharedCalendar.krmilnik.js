(function() {
  function sharedCalendarCtrl($rootScope, $location, sharedCalendarsStoritev, avtentikacija) {

    var vm = this;
    $rootScope.css = "/css/sharedCalendars.css";
    
    /*if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }*/

    
    sharedCalendarsStoritev.showAllShared().then(function success(odgovor){
        if(odgovor.data.length != 0){
            vm.calendars = odgovor.data; 
            vm.prikaziPrazno = "";
        }else{
            vm.prikaziPrazno = "yes";
        }
        
    },
    function error(odgovor){
        //napaka
    });
    
    
  }
  
  sharedCalendarCtrl.$inject = ["$rootScope", "$location", "sharedCalendarsStoritev", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("sharedCalendarCtrl", sharedCalendarCtrl);
})();