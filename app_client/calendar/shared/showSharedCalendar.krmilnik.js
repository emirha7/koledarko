(function() {
  function showSharedCalendarCtrl($rootScope, $location, $routeParams, podatkiKoledarja, avtentikacija) {
    
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }
    
    var vm = this;
    $rootScope.css = "/css/calendar.css";

    podatkiKoledarja.pridobiKoledarS($routeParams.idKoledarja).then(
        function success(odgovor){
          vm.imeKoledarja = odgovor.data[0].title;
          vm.data = odgovor.data[0];

          appendDynamicly(vm.data);
        },
        function error(odgovor){
          $location.path("/login");
        });
    
    var appendDynamicly = function(data){
        var array = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        
        
		var datum = new Date();

		if ($location.search().q != null) {
			if ($location.search().q == "" || isNaN(new Date($location.search().q))) {
				return;
			}
			datum = new Date($location.search().q);
		}
    
        var events = [];
		var dateMonth = datum.getMonth() + 1;
		var dateDay = datum.getDate();
		var dateYear = datum.getFullYear();
		var eventsSorted = sortDates(data.events);
		
		for (var i = 0; i < eventsSorted.length; i++) {
			var tempDatum = new Date(eventsSorted[i].date);
			var tempMonth = tempDatum.getMonth() + 1;
			var tempDay = tempDatum.getDate();
			var tempYear = tempDatum.getFullYear();
			if (dateMonth == tempMonth && dateYear == tempYear) {
				var tempA = tempDatum.getHours();
				var tempB = tempDatum.getMinutes();

				if (tempA < 10)
					tempA = "0" + tempA;


				if (tempB < 10)
					tempB = "0" + tempB;

				var ev = {
					startTime: tempA + ":" + tempB,
					day: tempDay
				};
				events.push(ev);
			}
		}
		
		function constructP(datum) {
        	var x = datum;
        	var year = x.getFullYear();
        	var s = getStartDay(x.getFullYear(), x.getMonth() + 1);
        	var z = datum.getMonth() + 1;
        	x.setDate(0);
        
        	return {
        		m: z,
        		y: year,
        		last: x.getDate(),
        		index: (s == 0) ? -1 : x.getDay()
        	};
        }
		
        function constructEventData(events, datum) {
        	var currentMY = getCurrentMonthYear(datum);
        	var numberOfDays = daysInMonth(currentMY.m, currentMY.y);
        	var hm = {};
        	var s = getStartDay(currentMY.y, currentMY.m);
        
        	for (var i = 1; i <= numberOfDays; i++) {
        		hm[i] = {
        			dday: i,
        			events: []
        		}
        	}
        
        	for (var i = 0; i < events.length; i++) {
        		hm[events[i].day].events.push(events[i].startTime);
        	}
        
        	hm.startDay = s;
        	hm.numberOfDays = numberOfDays;
        	return hm;
        }
        
        function getStartDay(y, m) {
        	var dateString = y + "-" + m + "-01";
        	var d = new Date(dateString);
        	return d.getDay();
        }
        
        function getCurrentMonthYear(datum) {
        	return {
        		m: datum.getMonth() + 1,
        		y: datum.getFullYear()
        	}
        }
        
        function daysInMonth(m, y) {
        	return new Date(y, m, 0).getDate();
        }
		
        function sortDates(array) {
        	return array.sort(function (a, b) {
        		return new Date(a.date) - new Date(b.date);
        	});
        }
        
        function getStartDay(y, m) {
        	var dateString = y + "-" + m + "-01";
        	var d = new Date(dateString);
        	return d.getDay();
        }
		
		var appendData = constructEventData(events, datum);
		var cPD = constructP(datum);
		var greyOnes = [];
		
		var arrayWeeks = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var j = 0;
		
		for (var i = appendData.startDay - 1; i >= 0 ; i--, j++) {
		    greyOnes.push({name: arrayWeeks[j], date: cPD.last - i});
		}
		
		var activeOnes = [];
		
		for (var i = 1; i <= appendData.numberOfDays; i++,j++){
		    appendData[i].name = arrayWeeks[j%7];
		    activeOnes.push(appendData[i]);
		}
		
		var lastOnes = [];
		var i = 1;
		while(j%7 != 0){
		    lastOnes.push({name: arrayWeeks[j%7], date: i});
		    j++;  
		    i++;
		}
		
		vm.greyOnes = greyOnes;
		vm.activeOnes = activeOnes;
		vm.lastOnes = lastOnes;
		vm.leto = cPD.y;
		vm.mesec = cPD.m;
        
    };
    
       var extractQuery = function(){
        	var q = $location.search().q;
        	if(typeof q === 'undefined'){
        	    return new Date();
        	}
        	return q;
        };
    
    
        vm.prevMonth = function(){
            var prev = new Date(extractQuery());
            prev.setDate(1);
            prev.setMonth(prev.getMonth()-1);
            $location.search("q", prev.getFullYear() + "-" + (prev.getMonth() + 1) + "-" + prev.getDate());
            $location.path("/sharedCalendars/" + $routeParams.idKoledarja);   
        }

        vm.nextMonth = function(){
            var next = new Date(extractQuery());
        	next.setDate(1);
        	next.setMonth(next.getMonth()+1);
        	$location.search("q", next.getFullYear() + "-" + (next.getMonth() + 1) + "-" + next.getDate());
        	$location.path("/sharedCalendars/" + $routeParams.idKoledarja);   
        }
  }
  
  showSharedCalendarCtrl.$inject = ["$rootScope", "$location", "$routeParams", "podatkiKoledarja", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("showSharedCalendarCtrl", showSharedCalendarCtrl);
})();