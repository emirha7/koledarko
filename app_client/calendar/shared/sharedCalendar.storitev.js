(function(){
    var sharedCalendarsStoritev = function($http){
        var showAllShared = function(){
            return $http.get("api/calendars/shared");
        };
        
        return {
            showAllShared: showAllShared      
        };
    };
    
    sharedCalendarsStoritev.$inject = ["$http"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("sharedCalendarsStoritev", sharedCalendarsStoritev);
})();