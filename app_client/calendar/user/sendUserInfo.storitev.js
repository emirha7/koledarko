(function(){
    var sendUserInfo = function($http, avtentikacija){
        var sendInfo = function(idOsebe, podatki){
            return $http.put("/api/users/" + idOsebe, podatki, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            sendInfo: sendInfo      
        };
    };
    
    sendUserInfo.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("sendUserInfo", sendUserInfo);
})();