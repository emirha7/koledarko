(function(){
    var getUserInfo = function($http, avtentikacija){
        var reqInfo = function(idOsebe){
            return $http.get("/api/users/" + idOsebe, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            reqInfo: reqInfo      
        };
    };
    
    getUserInfo.$inject = ["$http", "avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("getUserInfo", getUserInfo);
})();