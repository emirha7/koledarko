(function() {
  function userAccountCtrl($rootScope, $location, getUserInfo, sendUserInfo, avtentikacija) {
    
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }  
    
    var vm = this;
    $rootScope.css = "/css/profileSettings.css";
    
    vm.posljiPodatkeObrazca = function(){
       vm.napakaSporocilo = "";
       if(!vm.podatkiObrazca.confrimpassword || !vm.podatkiObrazca.nickname || !vm.podatkiObrazca.oldpassword || !vm.podatkiObrazca.newpassword){
         vm.napakaSporocilo = "Missing fields";
         return;
       }
       
       if(vm.podatkiObrazca.confrimpassword != vm.podatkiObrazca.newpassword){
          vm.napakaSporocilo = "Passwords do not match";
          return;
       }
       
       var podatki = {
         newpassword: vm.podatkiObrazca.newpassword,
         oldpassword: vm.podatkiObrazca.oldpassword,
         username: vm.username,
         password: vm.podatkiObrazca.newpassword,
         nickname: vm.podatkiObrazca.nickname,
         email: vm.email
       };

       sendUserInfo.sendInfo($rootScope.user, podatki).then(
         function success(odgovor){
           vm.nickname = odgovor.data.nickname;
           vm.podatkiObrazca.confrimpassword = "";
           vm.podatkiObrazca.nickname = "";
           vm.podatkiObrazca.oldpassword = "";
           vm.podatkiObrazca.newpassword = "";
         },
         function error(odgovor){
          vm.napakaSporocilo = "There was an error"; 
         });
    };
    
    getUserInfo.reqInfo($rootScope.user).then(
      function success(odgovor){
        vm.username = odgovor.data.username; 
        vm.password = odgovor.data.password;
        vm.nickname = odgovor.data.nickname;
        vm.email = odgovor.data.email;
      },
      function error(odgovor){
        $location.path("/login");
      });
  }
  
  userAccountCtrl.$inject = ["$rootScope", "$location", "getUserInfo", "sendUserInfo", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("userAccountCtrl", userAccountCtrl);
})();