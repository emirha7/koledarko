(function() {
  function dayPreviewCtrl($rootScope, $location, podatkiKoledarja, geolocationService, weatherService, avtentikacija) {
    
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }
    
    var vm = this;
    $rootScope.css = "/css/dayPreview.css";
    
    var appendingDataDynamicly = function(odgovor){
        var events = sortDates(odgovor.events);
		var currentDate = new Date($location.search().q);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();

		var e = [];
		for (var i = 0; i < events.length; i++) {
			var eventDate = new Date(events[i].date);
			var eventDay = eventDate.getDate();
			var eventMonth = eventDate.getMonth();
			var eventYear = eventDate.getFullYear();

            var ura = (new Date(events[i].date).getHours() < 10)?"0"+new Date(events[i].date).getHours():new Date(events[i].date).getHours();
            var minute = (new Date(events[i].date).getMinutes() < 10)?"0"+new Date(events[i].date).getMinutes():new Date(events[i].date).getMinutes();

			if (eventDay == currentDay && eventMonth == currentMonth && eventYear == currentYear) {
				e.push({id:events[i].id, title: events[i].title, date: ura+":"+minute});
			}
		}
		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		vm.calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};
		
		vm.events = e;
		vm.prenesiDatum = currentYear + "-" + (currentMonth+1) + "-" + currentDay;
		vm.eventAdd = "/calendar/" + $rootScope.calendar + "/event/add?q=" + currentYear + "-" + (currentMonth+1) + "-" + currentDay;
    };
    
    function sortDates(array) {
    	return array.sort(function (a, b) {
    		return new Date(a.date) - new Date(b.date);
    	});
    }
    
    podatkiKoledarja.pridobiKoledar($rootScope.calendar).then(
        function success(odgovor){
            appendingDataDynamicly(odgovor.data[0]);
        },
        function error(odgovor){
            //napaka
        });
        
    
    vm.cbError = function(){
        $rootScope.apply(function(){
           alert("PRISLO JE DO NAPAKE?"); 
        });
    };
    
    vm.cbNotSupported = function(){
        $rootScope.apply(function(){
           alert("browser not supported"); 
        });
    };
    
    vm.vreme = "";
    
    vm.callback = function(lokacija){
        var lat = lokacija.coords.latitude;
        var lng = lokacija.coords.longitude;
        
        var currentDate = new Date($location.search().q);
    	var currentDay = currentDate.getDate();
    	var currentMonth = currentDate.getMonth()+1;
    	var currentYear = currentDate.getFullYear();
        
   		if (currentMonth < 10)
			currentMonth = "0" + currentMonth;

		if (currentDay < 10)
			currentDay = "0" + currentDay;
        
        weatherService.getWeather(lat, lng, "1a2ac4e5ca3c476494c212254180812",currentYear+"-"+currentMonth+"-"+currentDay).then(
            function success(odgovor){
                vm.slika = "https:"+odgovor.data.current.condition.icon;
                vm.vreme = "true";
            }, function napaka(odgovor){
                //napaka
            });
    };
    
    geolocationService.getCurrentLocation(vm.callback, vm.cbError, vm.cbNotSupported);
  }
  
  dayPreviewCtrl.$inject = ["$rootScope", "$location", "podatkiKoledarja", "geolocationService", "weatherService", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("dayPreviewCtrl", dayPreviewCtrl);
})();