(function() {
  function searchCtrl($rootScope, $location, avtentikacija) {
      
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }
    var vm = this;
    $rootScope.css = "/css/search.css";
    
    vm.napakaSporocilo = "";
    
    vm.posljiPoizvedbo = function(){
        if(!vm.podatkiObrazca.lokacija){
            vm.napakaSporocilo = "Missing fields"
        }else{
            vm.napakaSporocilo = "";
            $location.search("p","0");
            $location.search("location",vm.podatkiObrazca.lokacija);
            $location.path("/searchResults");
        }
    }
  }
  
  searchCtrl.$inject = ["$rootScope", "$location","avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("searchCtrl", searchCtrl);
})();