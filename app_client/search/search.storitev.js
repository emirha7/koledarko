(function(){
    var searchEvents = function($http, avtentikacija){
        var isciEvente = function(page, location){
            return $http.get("/api/events/shared/filter2?p="+page+"&location="+location, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            isciEvente: isciEvente      
        };
    };
    
    searchEvents.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("searchEvents", searchEvents);
})();