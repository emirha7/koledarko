(function() {
  function searchResultsCtrl($rootScope, $location, searchEvents,avtentikacija) {
      
    if(avtentikacija.getToken() == null){
        $location.path("/login");
        return;
    }
    var vm = this;
    $rootScope.css = "/css/showSearch.css";
    
    vm.hasResults = "";
    
    var page = 0;
    var location = "";
    
    if($location.search().p){
        page = $location.search().p;
    }
    
    if($location.search().location){
        location = $location.search().location;
    }
    
    vm.hidePrev = "";
    vm.hideNext = "";
    
    if(page <= 0){
        vm.hidePrev = "true";
    }
    
    page--;
    vm.prevSearch = "?p="+page+"&location="+location;
    page+=2;
    vm.nextSearch = "?p="+page+"&location="+location;
    
    searchEvents.isciEvente($location.search().p,$location.search().location).then(
        function success(odgovor){

            if(odgovor.data.length < 10){
                vm.hideNext = "true";
            }else{
                vm.hideNext = "";
            }
            
            for (var i = 0; i < odgovor.data.length; i++) {
                odgovor.data[i].startTime = new Date(odgovor.data[i].startTime);
            }
            vm.events = odgovor.data;
        },
        function error(odgovor){
            console.log("error");
        });
    
  }
  
  searchResultsCtrl.$inject = ["$rootScope", "$location", "searchEvents", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("searchResultsCtrl", searchResultsCtrl);
})();