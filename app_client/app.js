(function(){
    function nastavitev($routeProvider, $locationProvider, $sceDelegateProvider) {
      $routeProvider
        .when('/login', {
            templateUrl: 'auth/login.pogled.html',
            controller: 'loginCtrl',
            controllerAs: 'vm'
        })
        .when('/register', {
            templateUrl: 'auth/register.pogled.html',
            controller: 'registerCtrl',
            controllerAs: 'vm'
        })
        .when('/calendar/:idKoledarja', {
            templateUrl: 'calendar/main/calendar.pogled.html',
            controller: 'mainCalendarCtrl',
            controllerAs: 'vm'
        })
        .when('/account/:idRacuna', {
            templateUrl: 'calendar/user/user.pogled.html',
            controller: 'userAccountCtrl',
            controllerAs: 'vm'
        })
        .when('/sharedCalendars', {
            templateUrl: 'calendar/shared/sharedCalendar.pogled.html',
            controller: 'sharedCalendarCtrl',
            controllerAs: 'vm'
        })
        .when('/sharedCalendars/:idKoledarja', {
            templateUrl: 'calendar/shared/showSharedCalendar.pogled.html',
            controller: 'showSharedCalendarCtrl',
            controllerAs: 'vm'
        })
        .when('/sharedCalendars/:idKoledarja/event', {
            templateUrl: 'calendar/shared/sharedCalendarPreview.pogled.html',
            controller: 'sharedCalendarPreviewCtrl',
            controllerAs: 'vm'
        })
        .when('/sharedEvents', {
            templateUrl: 'calendar/sharedEvents/sharedEvents.pogled.html',
            controller: 'sharedEventsCtrl',
            controllerAs: 'vm'
        })
        .when('/sharedEvents/:idEventa', {
            templateUrl: 'calendar/sharedEvents/sharedEventsPreview.pogled.html',
            controller: 'sharedEventsPreviewCtrl',
            controllerAs: 'vm'
        })
        .when('/calendar/:idKoledarja/event', {
            templateUrl: 'calendar/dayPreview/dayPreview.pogled.html',
            controller: 'dayPreviewCtrl',
            controllerAs: 'vm'
        })
        .when('/calendar/:idKoledarja/event/add', {
            templateUrl: 'calendar/event/eventAdd.pogled.html',
            controller: 'eventAddCtrl',
            controllerAs: 'vm'
        })
        .when('/calendar/:idKoledarja/event/:idEventa', {
            templateUrl: 'calendar/event/eventEdit.pogled.html',
            controller: 'eventEditCtrl',
            controllerAs: 'vm'
        })
        .when('/search', {
            templateUrl: 'search/search.pogled.html',
            controller: 'searchCtrl',
            controllerAs: 'vm'
        })
        .when('/searchResults', {
            templateUrl: 'search/searchResults.pogled.html',
            controller: 'searchResultsCtrl',
            controllerAs: 'vm'
        })
        .when('/logout', {
            templateUrl: 'logout/logout.pogled.html',
            controller: 'logoutCtrl',
            controllerAs: 'vm'
        })
         .when('/db', {
            templateUrl: 'db/db.pogled.html',
            controller: 'dbCtrl',
            controllerAs: 'vm'
        })
        .otherwise({redirectTo: '/login'});
        
        $sceDelegateProvider.resourceUrlWhitelist([
          'self', 'https://api.apixu.com/v1/**'
        ]);
        
        $locationProvider.html5Mode(true);
    }
    
    /* global angular */
    angular
        .module("koledarko", ['ngRoute', 'ui.bootstrap'])
        .config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', nastavitev]);
})();