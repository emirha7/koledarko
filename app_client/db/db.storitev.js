(function(){
    var dbDrop = function($http, avtentikacija){
        var dropKoledar = function(){
            return $http.delete("/api/database/", {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            dropKoledar: dropKoledar      
        };
    };
    
    dbDrop.$inject = ["$http", "avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("dbDrop", dbDrop);
})();