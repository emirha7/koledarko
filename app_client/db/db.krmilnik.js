(function() {
  function dbCtrl($rootScope, $location, dbDrop, sendEventData) {
     $rootScope.css = "/css/login.css";
     var vm = this;
    
     vm.oddajObrazec = function(){
        dbDrop.dropKoledar().then(function success(odgovor){
        },
        function error(odgovor){
            console.log("error");
        });
     }
     
     vm.insertPrecodedData = function(){
        if(!vm.myCalendar){
            vm.napakaSporocilo = "Focus man FOCUS";
            return;
        }
        
        for(var i = 0; i < 30; i++){
            var podatek = {
                endTime: "2018-12-01T05:00:00.000Z",
                location: "Ljubljana",
                note: "Tuc tuc" + i,
                shared: "true",
                startTime: "2018-12-01T19:00:00.000Z",
                title: i + "Party hard" + i
            };
            
            sendEventData.posljiData(vm.myCalendar, podatek).then(
        	    function success(odgovor){
        	        vm.napakaSporocilo = "";
        	        vm.uspeh = "true";
        	    },
        	    function error(odgovor){
        	        vm.napakaSporocilo = "There was an error";
        	    });
            
        }
     };
     
  }
  
  dbCtrl.$inject = ["$rootScope", "$location", "dbDrop", "sendEventData"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("dbCtrl", dbCtrl);
})();