(function() {
  var navigacija = function() {
    return {
      restrict: 'EA',
      templateUrl: '/skupno/directive/navigacija/navigation.directive.html'
    };
  };

  /* global angular */
  angular
    .module('koledarko')
    .directive('navigacija', navigacija);
})();  