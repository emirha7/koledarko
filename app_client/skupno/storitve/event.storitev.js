(function(){
    var eventDetail = function($http,avtentikacija){
        var getDetails = function(event){
            return $http.get("/api/events/" + event, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            getDetails: getDetails      
        };
    };
    
    eventDetail.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("eventDetail", eventDetail);
})();