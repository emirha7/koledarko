(function(){
    var sendEventData = function($http,avtentikacija){
        var posljiData = function(koledar, podatki){
            return $http.post("/api/events/" + koledar, podatki, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            posljiData: posljiData      
        };
    };
    
    sendEventData.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("sendEventData", sendEventData);
})();