(function(){
    var podatkiKoledarja = function($http, avtentikacija){
        var pridobiKoledar = function(koledar){
            console.log(avtentikacija.getToken());
            console.log(koledar);
            return $http.get('/api/mycalendar/' + koledar,  {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        var pridobiKoledarS = function(koledar){
            console.log(avtentikacija.getToken());
            console.log(koledar);
            return $http.get('/api/sharedcalendars/' + koledar);
        };
        
        
        return {
            pridobiKoledar: pridobiKoledar,  
            pridobiKoledarS: pridobiKoledarS
        };
    };
    
    podatkiKoledarja.$inject = ["$http", "avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("podatkiKoledarja", podatkiKoledarja);
})();