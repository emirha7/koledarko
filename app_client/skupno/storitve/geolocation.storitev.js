(function(){
    var geolocationService = function(){
        var getCurrentLocation = function(cbSuccess, cbError, cbNotSupported){
            /* global navigator */
           if(navigator.geolocation){
               navigator.geolocation.getCurrentPosition(cbSuccess, cbError);
           }else{
               cbNotSupported();
           }
        };
        
        return {
            getCurrentLocation: getCurrentLocation      
        };
    };
    
    /* global angular */
    angular
        .module("koledarko")
        .service("geolocationService", geolocationService);
})();