(function(){
    var updateEventData = function($http,avtentikacija){
        var posljiData = function(koledar, event, podatki){
            return $http.put("/api/events/" + koledar + "/" + event, podatki, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            posljiData: posljiData      
        };
    };
    
    updateEventData.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("updateEventData", updateEventData);
})();