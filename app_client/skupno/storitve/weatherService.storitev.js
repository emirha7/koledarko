(function (){
    var weatherService = function($http){
        
        var getWeather = function(lat, lng, key, datum){
            return $http.get("https://api.apixu.com/v1/forecast.json?key="+key+"&q="+lat+","+lng+"&dt="+datum);
        };
        
        
        return {
            getWeather: getWeather
        };   
    };
    
    weatherService.$inject = ['$http'];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("weatherService", weatherService);
    
})() 