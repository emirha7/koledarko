(function(){
    var deleteEvent = function($http, avtentikacija){
        var deleteData = function(koledar, event){
            return $http.delete("/api/events/" + koledar + "/" + event, {
                headers: {
                  Authorization: 'Bearer ' + avtentikacija.getToken()
                }
             });
        };
        
        return {
            deleteData: deleteData      
        };
    };
    
    deleteEvent.$inject = ["$http","avtentikacija"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("deleteEvent", deleteEvent);
})();