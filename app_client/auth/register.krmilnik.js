(function() {
  function registerCtrl($rootScope, $location, registracija, avtentikacija) {
    var vm = this;
    $rootScope.css = "/css/signup.css";
    
    vm.izvediRegistracijo = function(){
      
      var podatki = {
        username: vm.podatkiObrazca.username,
        email: vm.podatkiObrazca.email,
        password: vm.podatkiObrazca.password,
        password_repeat: vm.podatkiObrazca.password_repeat
      };
      
      
      if(!vm.podatkiObrazca.username || !vm.podatkiObrazca.email || !vm.podatkiObrazca.password || !vm.podatkiObrazca.password_repeat){
        vm.napakaSporocilo = "Missing fields";
      }else if(podatki.password != podatki.password_repeat){
        vm.napakaSporocilo = "Passwords do not match";
      }else{
       registracija.posljiRegistracijo(podatki).then(
        function success(odgovor){
          avtentikacija.saveToken(odgovor.data.zeton);

          $location.path("/login");
        },
        function error(odgovor){
          vm.napakaSporocilo = "There was an error";
          //console.log(odgovor.e); 
        });  
      }
    }
  }
  
  registerCtrl.$inject = ["$rootScope", "$location", "registracija", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("registerCtrl", registerCtrl);
})();