(function(){
    var loginUporabnika = function($http){
        var posljiLogin = function(username, password){
            return $http.post("/api/users/login",{username: username, password: password});
        };
        return {
            posljiLogin: posljiLogin      
        };
    };
    
    loginUporabnika.$inject = ["$http"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("loginUporabnika", loginUporabnika);
})();