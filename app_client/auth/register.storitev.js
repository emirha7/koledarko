(function(){
    var registracija = function($http){
        var posljiRegistracijo = function(podatki){
            return $http.post("api/users/register", podatki);
        };
        
        return {
            posljiRegistracijo: posljiRegistracijo      
        };
    };
    
    registracija.$inject = ["$http"];
    
    /* global angular */
    angular
        .module("koledarko")
        .service("registracija", registracija);
})();