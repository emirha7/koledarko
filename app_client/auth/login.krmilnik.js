(function() {
  function loginCtrl($rootScope, $location, loginUporabnika, avtentikacija) {
    
    var vm = this;
    $rootScope.css = "/css/login.css";
    
    vm.izvediLogin = function(){
    vm.napakaSporocilo = "";
    
      if(!vm.podatkiObrazca.username || !vm.podatkiObrazca.password){
        vm.napakaSporocilo = "Missing fields";
      }else{
       loginUporabnika.posljiLogin(vm.podatkiObrazca.username, vm.podatkiObrazca.password).then(
        function success(odgovor){
          vm.napakaSporocilo = "";

          if(odgovor.data){
            
            var z = JSON.parse(atob(odgovor.data.zeton.split(".")[1]));
            var idKoledarja = z.idKoledarja;
            
            var redirectUrl = "/calendar/" + idKoledarja;
			
            avtentikacija.saveToken(odgovor.data.zeton);
            $rootScope.calendar = idKoledarja;
            $rootScope.user = z._id;
            
            $location.path(redirectUrl); 
          }
        },
        function error(odgovor){
          vm.napakaSporocilo = "Wrong username or password";
        });  
      }
    }
  }
  
  loginCtrl.$inject = ["$rootScope", "$location", "loginUporabnika", "avtentikacija"];
  
  /* global angular, koledarkoapp */
  angular.module("koledarko").controller("loginCtrl", loginCtrl);
})();