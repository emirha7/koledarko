(function(){
    function avtentikacija($window){
        
        var saveToken = function(token){
            $window.localStorage['koledarko'] = token;
        };
        
        var getToken = function(){
            return $window.localStorage['koledarko'];
        };
        
         var odjava = function(cb) {
             $window.localStorage.removeItem('koledarko');
			 cb();
        };
        
        var jePrijavljen = function(){
            var token = getToken();
            if(token){
                var content = JSON.parse($window.atob(token.split(".")[1]));
                return content.datumPoteka > Date.now() / 1000;
            }else{
                return false;
            }
        }
        
        var currentUserInfo = function(){
            if(jePrijavljen()){
                var zeton = getToken();
                var content = JSON.parse($window.atob(zeton.split(".")[1]));
                return {
                    idKoledarja: content.idKoledarja,
                    idUporabnika: content._id
                };
            }
        }
        
        return {
            saveToken: saveToken,
            getToken: getToken,
            odjava: odjava,
            jePrijavljen: jePrijavljen,
            currentUserInfo: currentUserInfo
        };
        
    }
    
    avtentikacija.$inject = ['$window'];
    
    /*global angular */
    angular
        .module('koledarko')
        .service('avtentikacija', avtentikacija);
    
})();