(function() {
  function calendarEditModalnoOkno($uibModalInstance, podrobnostiKoledarja, saveCalendar) {
    var vm = this;

    vm.data = podrobnostiKoledarja.data;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      shrani: function() {
        if(!vm.podatkiObrazca || !vm.podatkiObrazca.description || !vm.podatkiObrazca.title){
            vm.napakaSporocilo = "Missing fields";
            return;
        }
          
        var podatki = {
            _id: vm.data._id,
            shared: vm.data.shared,
            title: vm.podatkiObrazca.title,
            description: vm.podatkiObrazca.description,
            user: vm.data.user,
            events: vm.data.events
        };
        
        saveCalendar.saveCal(vm.data._id, podatki).then(
            function success(odgovor){
                $uibModalInstance.close(odgovor.data);        
            },
            function error(odgovor){
                vm.napakaSporocilo = "There was an error";   
            });
        
      },
      share: function() {
          
        if(vm.data.shared == "false"){
           vm.data.shared = "true"; 
        }else{
            vm.data.shared = "false";
        }
        
        var podatki = {
            _id: vm.data._id,
            shared: vm.data.shared,
            title: vm.data.title,
            description: vm.data.description,
            user: vm.data.user,
            events: vm.data.events
        };
        
        saveCalendar.saveCal(vm.data._id, podatki).then(
            function success(odgovor){

                $uibModalInstance.close(odgovor.data);        
            },
            function error(odgovor){
                vm.napakaSporocilo = "There was an error";   
            });
          
        $uibModalInstance.close();
      },
      zapri: function() {
        $uibModalInstance.close();
      }
    };
  }
  
  calendarEditModalnoOkno.$inject = ['$uibModalInstance', "podrobnostiKoledarja", "saveCalendar"];

  /* global angular */
  angular
    .module('koledarko')
    .controller('calendarEditModalnoOkno', calendarEditModalnoOkno);
})();