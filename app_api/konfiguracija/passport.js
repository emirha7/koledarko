var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LokalnaStrategija({
    usernameField: 'username',
    passwordField: 'password'
  }, 
  function(uporabniskoIme, geslo, koncano) {
    User.findOne(
      {
        username: uporabniskoIme
      },
      function(napaka, user) {
        if (napaka)
          return koncano(napaka);
        if (!user) {
          return koncano(null, false, {
            sporocilo: 'Napačno uporabniško ime'
          });
        }
        if (!user.preveriGeslo(geslo)) {
          return koncano(null, false, {
            sporocilo: 'Napačno geslo'
          });
        }
        return koncano(null, user);
      }
    );
  }
));