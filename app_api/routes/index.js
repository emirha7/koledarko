var express = require('express');
var router = express.Router();
var ctrlCalendars = require('../controllers/calendars');
var ctrlUsers = require('../controllers/users');
var ctrlEvents = require('../controllers/events');
var ctrlWeather = require('../controllers/weather');
var ctrlDb = require('../controllers/database');
var ctrlAvtentikacija = require('../controllers/avtentikacija');


var jwt = require('express-jwt');
var avtentikacija = jwt({
  secret: process.env.JWT_GESLO,
  userProperty: 'payload'
});

router.delete('/database', ctrlDb.db);
  

router.get('/calendars/shared', ctrlCalendars.shared); //-------------------- i guess
router.get('/mycalendar/:idCalendar', avtentikacija, ctrlCalendars.calendarByID); //-------------------- i guess
router.get('/sharedcalendars/:idCalendar', ctrlCalendars.sharedcalendars); //-------------------- i guess

/*router.get('/calendars/shared',
  ctrlCalendars.shared);*/
/*router.get('/calendars/login', 
  ctrlCalendars.calendarUser); */

router.get('/calendars', ctrlCalendars.calendarAll); //-------------------- sam admin?
router.post('/calendars', 
  avtentikacija, ctrlCalendars.calendarCreate); //-------------------- sam admin ?
router.put('/calendars/:idCalendar', 
  avtentikacija,ctrlCalendars.calendarUpdate); //-------------------- sam svoj
router.delete('/calendars/:idCalendar', 
  avtentikacija, ctrlCalendars.calendarDelete); //-------------------- sam svoj

router.post('/users/register', ctrlAvtentikacija.register);
router.post('/users/login', ctrlAvtentikacija.login);

/*
router.get('/users/login', 
  ctrlUsers.userByUsername);
router.post('/users/register', 
  ctrlUsers.userRegister);*/
router.get('/users', ctrlUsers.userAll); //-------------------- sam admin ?
router.post('/users', 
  avtentikacija,ctrlUsers.userCreate); //-------------------- sam admin ?
router.get('/users/:idUser', 
  avtentikacija, ctrlUsers.userByID); //-------------------- sam admin ?
router.put('/users/:idUser', 
  avtentikacija, ctrlUsers.userUpdate); //-------------------- sam admin ?
router.delete('/users/:idUser', 
  avtentikacija, ctrlUsers.userDelete); //-------------------- sam admin ?

router.get('/events/shared/filter',
  ctrlEvents.sharedFilter);
router.get('/events/shared/filter2',
  ctrlEvents.sharedFilter2);
router.get('/events/shared',
  ctrlEvents.shared);
router.post('/events/:idCalendar', //------------- sam lastnik 
  avtentikacija, ctrlEvents.eventCreate);
router.get('/events/:idEvent', //------------- sam lastnik 
  avtentikacija,ctrlEvents.eventByID);
router.get('/events',  //------------- sam admin 
  avtentikacija, ctrlEvents.eventAll);
router.put('/events/:idCalendar/:idEvent', //------------- sam lastnik 
  avtentikacija, ctrlEvents.eventUpdate);
router.delete('/events/:idCalendar/:idEvent',  //------------- sam admin 
  avtentikacija, ctrlEvents.eventDelete);
  
  
router.get('/weather/all',
  ctrlWeather.weatherAll);
router.get('/weather', 
  ctrlWeather.weatherOnDate);
router.post('/weather', 
  ctrlWeather.weatherCreate);
module.exports = router;