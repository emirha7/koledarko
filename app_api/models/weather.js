var mongoose = require('mongoose');

var weatherSchema = new mongoose.Schema({
  longitude: {type: String},
  latitude: {type: String},
  description: {type: String},
  date: {type: Date}
});
mongoose.model('Weather', weatherSchema,'Weather');