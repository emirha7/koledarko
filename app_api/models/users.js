var mongoose = require('mongoose');
var crypto = require("crypto");
var jwt = require("jsonwebtoken");

var usersSchema = new mongoose.Schema({
  username: {type: mongoose.Schema.Types.String, unique : true, required: true, dropDups: true},
  nickname: {type: String, required: true},
  email: { type: String,required: true },
  zgoscenaVrednost: String,
  nakljucnaVrednost: String
});


usersSchema.methods.nastaviGeslo = function(geslo) {
  this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
  this.zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

usersSchema.methods.preveriGeslo = function(geslo) {
  var zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
  return this.zgoscenaVrednost == zgoscenaVrednost;
};


usersSchema.methods.generirajJwt = function(idKoledarja){
  var datumPoteka = new Date();
  datumPoteka.setDate(datumPoteka.getDate() + 7);
  console.log("id koledarja v funkciji je "+idKoledarja);
  return jwt.sign({
    _id: this._id,
    idKoledarja: idKoledarja,
    datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
  }, "toleNašeGeslo");
};


mongoose.model('User', usersSchema, 'Users');