var mongoose = require('mongoose');

var eventsSchema = new mongoose.Schema({
  title: {type: String, required: true},
  location: {type: String, required: true},
  startTime: {type: Date, required: true},
  endTime: {type: Date, required: true},
  note: String,
  shared: {type: String, default: "false"}
});
mongoose.model('Event', eventsSchema, 'Events');