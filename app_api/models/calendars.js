var mongoose = require('mongoose');

var calendarsSchema = new mongoose.Schema({
  title: {type: String, required: true},
  description: String,
  shared: {type: String, default: "false"},
  user: { type: mongoose.Schema.Types.ObjectId, ref:'Users', required: true},
  events: 
  [{ 
    id: {type: mongoose.Schema.Types.ObjectId, ref:'Events', required: true},
    date: {type: Date},
    title: {type: String}
  }]
});
mongoose.model('Calendar', calendarsSchema, 'Calendars');