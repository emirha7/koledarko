var mongoose = require('mongoose');
var User = mongoose.model('User');
var calendar = require('./calendars')

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};


module.exports.userAll = function (zahteva, odgovor) {
	User
		.find({})
		.exec(function (napaka, user) {
			if (!user) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No events."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {
				vrniJsonOdgovor(odgovor, 200, user);
			}
		});
};

module.exports.userCreate = function (zahteva, odgovor) {
	User.create({
		username: zahteva.body.username,
		password: zahteva.body.password,
		nickname: zahteva.body.nickname,
		email: zahteva.body.email
	}, function (napaka, user) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 201, user);
		}
	});
};

module.exports.userRegister = function (zahteva, odgovor) {
	User.create({
		username: zahteva.body.username,
		password: zahteva.body.password,
		nickname: zahteva.body.username,
		email: zahteva.body.email
	}, function (napaka, user) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			calendar.calendarRegister(zahteva, odgovor, user);
			vrniJsonOdgovor(odgovor, 201, user);
		}
	});
};

module.exports.userByID = function (zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idUser) {
		User
			.findById(zahteva.params.idUser)
			.exec(function (napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, user);
				}

			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};

module.exports.userByUsername = function (zahteva, odgovor, cal) {
	User
		.where('username').equals(zahteva.query.username)
		.where('password').equals(zahteva.query.password)
		.exec(function (napaka, user) {
			if (!user) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No events."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {
				if (user.length == 0) {
					vrniJsonOdgovor(odgovor, 404, {
						status: "404",
						message: "Wrong username or password"
					});
				} else {
					cal(zahteva, odgovor, user);
				}
			}
		});
};

module.exports.userUpdate = function (zahteva, odgovor) {
	if (!zahteva.params.idUser) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
		return;
	}
	User
		.findById(zahteva.params.idUser)
		.exec(
			function (napaka, user) {
				if (!user) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No user with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				
				if(!zahteva.body.oldpassword){
					vrniJsonOdgovor(odgovor, 400, {
						status: 400,
						message: "Bad request"
					});
					return;
				}

				if(!user.preveriGeslo(zahteva.body.oldpassword)){
					vrniJsonOdgovor(odgovor, 400, {
						status: 400,
						message: "Bad request"
					});
					return;
				}

				user.username = zahteva.body.username;
				user.nastaviGeslo(zahteva.body.newpassword);
				user.nickname = zahteva.body.nickname;
				user.email = zahteva.body.email;
				user.save(function (napaka, user) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, user);
					}
				});
			}
		);
};

module.exports.userDelete = function (zahteva, odgovor) {
	var idUser = zahteva.params.idUser;
	if (idUser) {
		User
			.findByIdAndRemove(idUser)
			.exec(
				function (napaka, user) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 404, napaka);
						return;
					}
					vrniJsonOdgovor(odgovor, 204, null);
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "User id must be given."
		});
	}
};