var mongoose = require('mongoose');
var Calendar = mongoose.model('Calendar');
var user = require('./users');
var Uporabnik = mongoose.model('User');

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};


module.exports.calendarAll = function (zahteva, odgovor) {
	Calendar
		.find({})
		.exec(function (napaka, calendar) {
			if (!calendar) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No calendars."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {
				vrniJsonOdgovor(odgovor, 200, calendar);
			}
		});
};

module.exports.shared = function (zahteva, odgovor) {
	Calendar
		.find({
			shared: "true"
		})
		.exec(function (napaka, event) {
			if (!event) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No shared events."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {
				vrniJsonOdgovor(odgovor, 200, event);
			}
		});
};
// GLEJ SPODI !!
/*module.exports.calendarByID = function (zahteva, odgovor) {
	if (zahteva.params && zahteva.params.idCalendar) {
		Calendar
			.findById(zahteva.params.idCalendar)
			.exec(function (napaka, calendar) {
				if (!calendar) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No calendar with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
				} else {
					vrniJsonOdgovor(odgovor, 200, calendar);
				}

			});
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};*/





module.exports.calendarUser = function (zahteva, odgovor) {
	if (zahteva.query && zahteva.query.username && zahteva.query.password) {
		var cal = function (zahteva, odgovor, user) {
			Calendar
				.where('user').equals(user[0]._id)
				.exec(function (napaka, calendar) {
					if (!calendar) {
						vrniJsonOdgovor(odgovor, 404, {
							status: 404,
							message: "No calendar with username and password."
						});
						return;
					} else if (napaka) {
						vrniJsonOdgovor(odgovor, 500, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, calendar);
					}
				});
		}
		user.userByUsername(zahteva, odgovor, cal);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing username and/or password."
		});
	};
};



module.exports.calendarUser2 = function (zahteva, odgovor, user) {
	Calendar
		.where('user').equals(user._id)
		.exec(function (napaka, calendar) {
			if (!calendar) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No calendar with username and password."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {

				if(calendar.length == 0){
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No calendar with username and password."
					});
					return;
				}
				
				var zeton = user.generirajJwt(calendar[0]._id);
				vrniJsonOdgovor(odgovor, 200, {
					"zeton": zeton
				});
			}
		});
};




module.exports.calendarCreate = function (zahteva, odgovor) {
	Calendar.create({
		title: zahteva.body.title,
		description: zahteva.body.description,
		user: zahteva.body.user,
		events: zahteva.body.events
	}, function (napaka, calendar) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 201, calendar);
		}
	});
};

module.exports.calendarRegister = function (zahteva, odgovor, user) {

	Calendar.create({
		title: "Your calendar title",
		description: "Your calendar description",
		user: user._id,
		events: []
	}, function (napaka, calendar) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			var zeton = user.generirajJwt(calendar._id);

			 vrniJsonOdgovor(odgovor, 200, {
    			"zeton": zeton
    		 });
		}
	});
};

module.exports.calendarAddEvent = function (zahteva, odgovor, event) {
	Calendar
		.findById(zahteva.params.idCalendar)
		.exec(
			function (napaka, calendar) {
				if (!calendar) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No event with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				calendar.events.push({
					id: event._id,
					date: event.startTime,
					title: event.title
				});
				calendar.save(function (napaka, calendar) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, calendar);
					}
				});
			}
		);
};

module.exports.calendarUpdateEvent = function (zahteva, odgovor, event, calendar_id) {
	Calendar
		.findById(calendar_id)
		.exec(
			function (napaka, calendar) {
				if (!calendar) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No calendar with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				for (var i = 0; i < calendar.events.length; i++) {
					if (String(calendar.events[i].id) == String(event._id)) {
						calendar.events[i].date = event.startTime;
						calendar.events[i].title = event.title;
						break;
					}
				}
				calendar.save(function (napaka, calendar) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, calendar);
					}
				});
			}
		);
};

module.exports.calendarRemoveEvent = function (zahteva, odgovor, event_id, calendar_id) {
	Calendar
		.findById(calendar_id)
		.exec(
			function (napaka, calendar) {
				if (!calendar) {
					vrniJsonOdgovor(odgovor, 404, {
						status: 404,
						message: "No calendar with given id."
					});
					return;
				} else if (napaka) {
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}
				for (var i = 0; i < calendar.events.length; i++) {
					if (calendar.events[i].id == event_id) {
						calendar.events.splice(i, 1);
						break;
					}
				}
				calendar.save(function (napaka, calendar) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 400, napaka);
					} else {
						vrniJsonOdgovor(odgovor, 200, calendar);
					}
				});
			}
		);
};


module.exports.calendarUpdate = function (zahteva, odgovor) {
	if (!zahteva.params.idCalendar) {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Calendar id must be given."
		});
		return;
	}
	getCurrentUser(zahteva, odgovor, userCbUpdate);
};



module.exports.calendarDelete = function (zahteva, odgovor) {
	var idCalendar = zahteva.params.idCalendar;
	if (idCalendar) {
		Calendar
			.findByIdAndRemove(idCalendar)
			.exec(
				function (napaka, calendar) {
					if (napaka) {
						vrniJsonOdgovor(odgovor, 404, napaka);
						return;
					}
					vrniJsonOdgovor(odgovor, 204, null);
				}
			);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Calendar id must be given."
		});
	}
};


var shared2 = function (zahteva, odgovor) {
	Calendar
		.find({
			shared: "true"
		})
		.exec(function (napaka, event) {
			if (!event) {
				vrniJsonOdgovor(odgovor, 404, {
					status: 404,
					message: "No shared events."
				});
				return;
			} else if (napaka) {
				vrniJsonOdgovor(odgovor, 500, napaka);
			} else {
				vrniJsonOdgovor(odgovor, 200, event);
			}
		});
};



module.exports.calendarByID = function (zahteva, odgovor) {

	if(zahteva.params.idCalendar == "shared"){
		shared2(zahteva, odgovor);
		return;
	}
	
	if (zahteva.params && zahteva.params.idCalendar) {
		getCurrentUser(zahteva, odgovor, userCb);
	} else {
		vrniJsonOdgovor(odgovor, 400, {
			status: 400,
			message: "Missing id."
		});
	};
};


var getCurrentUser = function(zahteva, odgovor, userCb){

	if(zahteva.payload && zahteva.payload._id){
		Uporabnik
			.findById(zahteva.payload._id)
			.exec(function(napaka, uporabnik){
				if(!uporabnik){
					vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem uporabnika!"});
					return;
				}
				if(napaka){
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				userCb(zahteva, odgovor, uporabnik);
				
			});
	}else{
		vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Ni podatka o uporabniku!"});
		return;
	}
}

/*


*/

var userCb = function(zahteva, odgovor, uporabnik){
	if(zahteva.payload && zahteva.payload._id){
		Calendar
			.find({})
			.exec(function(napaka, calendar){
				if(napaka){
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				if(calendar.length == 0){
					vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem koledarja!"});
						return;
				}
				
				if(!calendar){
					vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem koledarja!"});
					return;
				}
				
			
			
				for(var c in calendar){
					if(calendar[c]._id == zahteva.payload.idKoledarja){
							vrniJsonOdgovor(odgovor, 200, [calendar[c]]);
							return;
					}
				}
				
			
				
				
				vrniJsonOdgovor(odgovor, 401, {"sporočilo": "Nimaš pravice"});
				
			});
	}else{
		vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Ni podatka o uporabniku!"});
		return;
	}
}

var userCbUpdate = function(zahteva, odgovor, uporabnik){

	if(zahteva.payload && zahteva.payload._id){
		Calendar
			.where('user').equals(uporabnik._id)
			.exec(function(napaka, calendar){
				if(napaka){
					vrniJsonOdgovor(odgovor, 500, napaka);
					return;
				}

				if(!calendar){
					vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem koledarja!"});
					return;
				}
				
				if(calendar.length == 0){
						vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem koledarja!"});
					return;
				}

				if(calendar[0].user == zahteva.payload._id){
					//vrniJsonOdgovor(odgovor, 200, calendar);
					realUpdate(zahteva, odgovor, calendar[0]);
					return;
				}
				
				vrniJsonOdgovor(odgovor, 401, {"sporočilo": "Nimaš pravice"});
				
			});
	}else{
		vrniJsonOdgovor(odgovor, 400, {"sporočilo": "Ni podatka o uporabniku!"});
		return;
	}
}

function realUpdate(zahteva, odgovor, calendar){
	calendar.shared = zahteva.body.shared;
	calendar.title = zahteva.body.title;
	calendar.description = zahteva.body.description;
	calendar.user = zahteva.body.user;
	calendar.events = zahteva.body.events;

	calendar.save(function (napaka, calendar) {
		if (napaka) {
			vrniJsonOdgovor(odgovor, 400, napaka);
		} else {
			vrniJsonOdgovor(odgovor, 200, calendar);
		}
	});
}

module.exports.sharedcalendars = function(zahteva, odgovor){
	if(zahteva.params.idCalendar!=null){
		Calendar
		.find({})
		.exec(function(napaka, calendars){
			if(napaka){
				vrniJsonOdgovor(odgovor, 500, napaka);
				return;
			}
			
			for(var c in calendars){
				if(calendars[c]._id == zahteva.params.idCalendar){
					vrniJsonOdgovor(odgovor, 200, [calendars[c]])
				}
			}
			
		});
	}else{
		vrniJsonOdgovor(odgovor,404,{"Sporočilo": "Bad request"});
	}
}