var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

var calendar = require("./calendars");

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.register = function(zahteva, odgovor) {
  
  if(zahteva.body.password_repeat != zahteva.body.password){
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Gesli se ne ujemata!"
    });
    return;
  }
  
  if (!zahteva.body.username || !zahteva.body.email || !zahteva.body.password) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  } else if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(zahteva.body.email))) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Elektronski naslov je neustrezen!"
    });
    return;
  }
  var user = new User();
  user.username = zahteva.body.username;
  user.nickname = zahteva.body.username;
  user.email = zahteva.body.email;
  user.nastaviGeslo(zahteva.body.password);
  user.save(function(napaka) {
   if (napaka) {
     vrniJsonOdgovor(odgovor, 500, napaka);
   } else {
     calendar.calendarRegister(zahteva, odgovor, user);
    
   }
  });
};

module.exports.login = function(zahteva, odgovor) {
  if(!zahteva.body.username || !zahteva.body.password){
     vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Zahtevani so vsi podatki"
    });
    return;
  }
  
  passport.authenticate('local', function(napaka, user, podatki){
    if(napaka){
      vrniJsonOdgovor(odgovor, 404, napaka);
      return;
    }
    if(user){
      calendar.calendarUser2(zahteva, odgovor, user);
    }else{
      vrniJsonOdgovor(odgovor, 401, podatki);
    }
  })(zahteva, odgovor);
  
};