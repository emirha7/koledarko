var mongoose = require('mongoose');
var Calendar = mongoose.model('Calendar');
var Event = mongoose.model('Event');
var Weather = mongoose.model('Weather');
var User = mongoose.model('User');

var dbURI = 'mongodb://localhost/sp_projekt';

if (process.env.NODE_ENV === 'production') {
	dbURI = process.env.MLAB_URI;
}

if (process.env.DB_URI) {
	dbURI = process.env.DB_URI;
}

var vrniJsonOdgovor = function (odgovor, status, vsebina) {
	odgovor.status(status);
	odgovor.json(vsebina);
};


module.exports.db = function (zahteva, odgovor) {
	User.remove({}, function (err) {
		if (err) {
            vrniJsonOdgovor(odgovor,400, err);
		} else {
			Weather.remove({}, function (err) {
				if (err) {
                    vrniJsonOdgovor(odgovor,400, err);
				} else {
					Calendar.remove({}, function (err) {
						if (err) {
                            vrniJsonOdgovor(odgovor,400, err);
						} else {
							Event.remove({}, function (err) {
    							if (err) {
    							    vrniJsonOdgovor(odgovor,400, err); 
    							} else {
    							    vrniJsonOdgovor(odgovor,200, {message: "Database droped"});
    							}
							});
						}
					});
				}
			});
		}
	});
};