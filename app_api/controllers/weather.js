var mongoose = require('mongoose');
var Weather = mongoose.model('Weather');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.weatherAll = function(zahteva, odgovor) {
    Weather
        .find({})
        .exec(function(napaka,user){
            if(!user){
                vrniJsonOdgovor(odgovor,404,{
                    status: 404,
                    message: "No weather."
                });
                return;
            }else if(napaka) {
                vrniJsonOdgovor(odgovor,500,napaka);
            }else{
                vrniJsonOdgovor(odgovor, 200, user);
            }
    });
};

module.exports.weatherCreate = function(zahteva, odgovor) {
 Weather.create({
        longitude: zahteva.body.longitude,
        latitude: zahteva.body.latitude,
        description: zahteva.body.description,
        date: zahteva.body.date
    }, function(napaka,weather){
        if(napaka){
            vrniJsonOdgovor(odgovor,400,napaka);
        }else{
            vrniJsonOdgovor(odgovor,201,weather);
        }
    });
};

module.exports.weatherOnDate = function(zahteva, odgovor) {
    var year = zahteva.query.year;
    var month = zahteva.query.month;
    var day = zahteva.query.day;
    var longitude = parseFloat(zahteva.query.longitude);
    var latitude = parseFloat(zahteva.query.latitude);
    var result = [];
    var date1 = new Date(year+"-"+month+"-"+day+"T00:00:00.000Z");
    Weather
        .find({date: date1})
        .exec(function(napaka,weather){
            if(!weather){
                vrniJsonOdgovor(odgovor,404,{
                    status: 404,
                    message: "No weather."
                });
                return;
            }else if(napaka) {
                vrniJsonOdgovor(odgovor,500,napaka);
            }else{
                for (var i = 0; i < weather.length; i++){
                    if (Math.abs(parseFloat(weather[i].longitude) - longitude) < 1 && Math.abs(parseFloat(weather[i].latitude) - latitude) < 1) { 
                        result = weather[i];
                        break;
                    }
                }
                vrniJsonOdgovor(odgovor, 200, result);
            }
    });
};