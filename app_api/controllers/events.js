var mongoose = require('mongoose');
var Event = mongoose.model('Event');
var calendar = require('./calendars');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.eventCreate = function(zahteva, odgovor) {
    Event.create({
        shared: zahteva.body.shared,
        title: zahteva.body.title,
        location: zahteva.body.location,
        startTime: zahteva.body.startTime,
        endTime: zahteva.body.endTime,
        note: zahteva.body.note
    }, function(napaka,event){
        if(napaka){
            vrniJsonOdgovor(odgovor,400,napaka);
        }else{
            calendar.calendarAddEvent(zahteva,odgovor,event);
        }
    });
};

module.exports.eventByID = function(zahteva, odgovor) {
    if (zahteva.params && zahteva.params.idEvent) {
        Event
            .findById(zahteva.params.idEvent)
            .exec(function(napaka,event){
                if(!event){
                    vrniJsonOdgovor(odgovor,404,{
                        status: 404, 
                        message: "No event with given id."
                    });
                    return;
                }else if(napaka) {
                    vrniJsonOdgovor(odgovor,500,napaka);
                }else{
                    vrniJsonOdgovor(odgovor,200, event);
                }
                
            });
    } else {
        vrniJsonOdgovor(odgovor, 400, {
          status: 404,
          message: "Missing id."
        });
    };
};

module.exports.shared = function(zahteva, odgovor) {
  Event
  //.$where('shared == false')
  .find({shared: "true"})
  //.where('shared').equals('false')
      //.aggregate([{ $match: { shared : "false" } }])
      .exec(function(napaka,event){
          if(!event){
              vrniJsonOdgovor(odgovor,404,{
                  status: 404, 
                  message: "No shared events."
              });
              return;
          }else if(napaka) {
              vrniJsonOdgovor(odgovor,500,napaka);
          }else{
              vrniJsonOdgovor(odgovor,200, event);
          }
   });
};

module.exports.eventAll = function(zahteva, odgovor) {
    Event
        .find({})
        .exec(function(napaka,event){
            if(!event){
                vrniJsonOdgovor(odgovor,404,{
                    status: 404,
                    message: "No events."
                });
                return;
            }else if(napaka) {
                vrniJsonOdgovor(odgovor,500,napaka);
            }else{
                vrniJsonOdgovor(odgovor, 200, event);
            }
        });
};

module.exports.eventUpdate = function(zahteva, odgovor) {
  var idEvent = zahteva.params.idEvent;
  var idCalendar = zahteva.params.idCalendar;
  if (!idEvent || !idCalendar) {
    vrniJsonOdgovor(odgovor, 400, {
      status: 400,
      message: "Event and calendar id must be given."
    });
    return;
  }
  Event
    .findById(idEvent)
    .exec(
      function(napaka, event) {
        if (!event) {
          vrniJsonOdgovor(odgovor, 404, {
            status: 404,
            message: "No event with given id."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        event.title = zahteva.body.title;
        event.location = zahteva.body.location;
        event.startTime = zahteva.body.startTime;
        event.endTime = zahteva.body.endTime;
        event.note = zahteva.body.note;
        event.shared = zahteva.body.shared;
        event.save(function(napaka,event) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            calendar.calendarUpdateEvent(zahteva,odgovor,event,idCalendar);
          }
        });
      }
    );
};

module.exports.sharedFilter = function(zahteva, odgovor) {
  Event
  .find({shared: "true"})
  .where('location').equals(zahteva.query.location)
      .exec(function(napaka,event){
          if(!event){
              vrniJsonOdgovor(odgovor,404,{
                  status: 404, 
                  message: "No shared events."
              });
              return;
          }else if(napaka) {
              vrniJsonOdgovor(odgovor,500,napaka);
          }else{
              vrniJsonOdgovor(odgovor,200, event);
          }
   });
};

module.exports.sharedFilter2 = function(zahteva, odgovor) {
  Event
  .find({shared: "true"})
  .skip(zahteva.query.p * 10)
  .limit(10)
  .where('location').equals(zahteva.query.location)
      .exec(function(napaka,event){
          if(!event){
              vrniJsonOdgovor(odgovor,404,{
                  status: 404, 
                  message: "No shared events."
              });
              return;
          }else if(napaka) {
              vrniJsonOdgovor(odgovor,500,napaka);
          }else{
              vrniJsonOdgovor(odgovor,200, event);
          }
   });
}; 


module.exports.eventDelete = function(zahteva, odgovor) {
  var idEvent = zahteva.params.idEvent;
  var idCalendar = zahteva.params.idCalendar;
  if (idEvent) {
    Event
      .findByIdAndRemove(idEvent)
      .exec(
        function(napaka, event) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          calendar.calendarRemoveEvent(zahteva,odgovor,idEvent,idCalendar);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      status: 400,
      message: "Event id must be given."
    });
  }
};