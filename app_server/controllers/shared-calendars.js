var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.calendar = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.errors != null || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
		}
		res.render('sharedCalendars', {
			title: 'Koledarko',
			cssFile: 'sharedCalendars',
			content: content
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/shared",
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.previewCalendar = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
		} else if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
		} else {
			var datum = new Date();

			if (req.query.q != null) {
				if (req.query.q == "" || isNaN(new Date(req.query.q))) {
					var error = {
						status: "400",
						message: "Missing date in URL."
					};
					res.render('errorPage', {
						error: error
					});
					return;
				}
				datum = new Date(req.query.q);
			}

			var events = [];
			var dateMonth = datum.getMonth() + 1;
			var dateDay = datum.getDate();
			var dateYear = datum.getFullYear();
			content.events = sortDates(content.events);
			for (var i = 0; i < content.events.length; i++) {
				var tempDatum = new Date(content.events[i].date);
				var tempMonth = tempDatum.getMonth() + 1;
				var tempDay = tempDatum.getDate();
				var tempYear = tempDatum.getFullYear();
				if (dateMonth == tempMonth && dateYear == tempYear) {
					var tempA = tempDatum.getHours();
					var tempB = tempDatum.getMinutes();

					if (tempA < 10)
						tempA = "0" + tempA;


					if (tempB < 10)
						tempB = "0" + tempB;

					var ev = {
						startTime: tempA + ":" + tempB,
						day: tempDay
					};
					events.push(ev);
				}
			}
			var appendData = constructEventData(events, datum);
			res.render('sharedCalandarPreview', {
				contentD: content,
				cssFile: 'calendar',
				data: appendData,
				dataP: constructP(datum)
			});
		}
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.previewCalendarEvents = function (req, res) {
	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "400",
			message: "Missing date in URL."
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		var events = sortDates(content.events);
		var currentDate = new Date(req.query.q);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();

		var e = [];
		for (var i = 0; i < events.length; i++) {
			var eventDate = new Date(events[i].date);
			var eventDay = eventDate.getDate();
			var eventMonth = eventDate.getMonth();
			var eventYear = eventDate.getFullYear();

			if (eventDay == currentDay && eventMonth == currentMonth && eventYear == currentYear) {
				e.push(events[i]);
			}
		}

		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		var calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};

		res.render('dayPreviewShared', {
			title: 'Koledarko',
			content: e,
			calendar: content._id,
			queryDate: req.query.q,
			cssFile: 'dayPreview',
			calendarInfo: calendarInfo
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
}


/* HELPER FUNCTIONS */

function constructP(datum) {
	var x = datum;
	var year = x.getFullYear();
	var s = getStartDay(x.getFullYear(), x.getMonth() + 1);
	var z = datum.getMonth() + 1;
	x.setDate(0);

	return {
		m: z,
		y: year,
		last: x.getDate(),
		index: (s == 0) ? -1 : x.getDay()
	};
}

function constructEventData(events, datum) {
	var currentMY = getCurrentMonthYear(datum);
	var numberOfDays = daysInMonth(currentMY.m, currentMY.y);
	var hm = {};
	var s = getStartDay(currentMY.y, currentMY.m);

	for (var i = 1; i <= numberOfDays; i++) {
		hm[i] = {
			dday: i,
			events: []
		}
	}

	for (var i = 0; i < events.length; i++) {
		hm[events[i].day].events.push(events[i].startTime);
	}

	hm.startDay = s;
	hm.numberOfDays = numberOfDays;
	return hm;
}

function getStartDay(y, m) {
	var dateString = y + "-" + m + "-01";
	var d = new Date(dateString);
	return d.getDay();
}

function getCurrentMonthYear(datum) {
	return {
		m: datum.getMonth() + 1,
		y: datum.getFullYear()
	}
}

function daysInMonth(m, y) {
	return new Date(y, m, 0).getDate();
}

function sortDates(array) {
	return array.sort(function (a, b) {
		return new Date(a.date) - new Date(b.date);
	});
}

function concatTimeAndDate(time, date) {
	var dateTime = time;
	var dateQuery = date;

	var d = new Date(dateQuery);
	var temp = dateTime.split(":");

	d.setHours(temp[0]);
	d.setMinutes(temp[1]);

	return d;
}

function checkReqBodyEvent(body) {
	if (body.title == null || body.title == "" || body.location == null || body.location == "" || body.startTime == null || !/^\d{2}:\d{2}$/.test(body.startTime) ||
		body.endTime == null || !/^\d{2}:\d{2}$/.test(body.endTime) || body.note == null || body.note == "") {

		return false;
	}
	return true;
}