var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.search = function (req, res) {
	res.render('search', {
		title: 'Koledarko',
		cssFile: 'search',
		calendar: req.params.idKoledarja
	});
};

module.exports.searchResults = function (req, res) {
	if (req.query.location == null || req.query.location == "") {
		var error = {
			status: "400",
			message: "Please insert parameters"
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		console.log(content);
		if (content.errmsg != null || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}
		res.render('showEvents', {
			title: 'Koledarko',
			cssFile: 'showSearch',
			calendar: req.params.idKoledarja,
			content: content
		});
	}

  console.log(req.query.location);

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/shared/filter?location=" + req.query.location,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};