var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.getWeather = function(req, res) {
  res.contentType('application/json');
  res.setHeader("Access-Control-Allow-Origin", "*");

  if(req.query.lat == null || req.query.long == null || isNaN(parseFloat(req.query.lat)) || isNaN(parseFloat(req.query.long)) || 
     req.query.month == null || isNaN(parseInt(req.query.month)) || req.query.day == null || isNaN(parseInt(req.query.day)) ||
     req.query.year == null || isNaN(parseInt(req.query.year))){
      
      res.json({error: "invalid parameters"});
      return;
  }

  var reqParams = {
    url: "https://api.apixu.com/v1/forecast.json?key=1a2ac4e5ca3c476494c212254180812&q="+req.query.lat+","+req.query.long+"&dt=",
    method: 'get',
    json: {}
  };
  
  //najprej poslj request, da marko pove, ce ma v bazi, ce ma pol posle on , 
  var m = req.query.month;
       
  if(req.query.month.length == 1){
    m = "0"+req.query.month;
  }
    
  var d = req.query.day.substring(0,req.query.day.length-1)
  if(d.length == 1){
     d = "0"+d;
  }
    
  var dateStr = "year="+req.query.year+"&month="+m+"&day="+d+"&longitude="+req.query.long+"&latitude="+req.query.lat;
  reqParams.url+=req.query.year+"-"+m+"-"+d;
  
  var getWeatherParam = {
     url:  paramsApi.server + paramsApi.apiURI + "/weather/?"+dateStr,
     method: 'get',
     json: {}
  };
  
  request(getWeatherParam, function(errWG, responseWG, contentWG){
      if(errWG){
         res.render('errorPage', {error: {status: errWG.status, message: errWG.message}}); 
      }else {
        if(contentWG.status != null){
            res.render('errorPage', {error: {status: errWG.status, message: errWG.message}});
            return;
        }
        
        if(contentWG.length == 0){
            apixuAndPost();
            return;
        }
        res.json(contentWG.description);
      }
  });
  
  //ce pa nima pol to dela 
  var apixuAndPost = function (){
      request(reqParams, function(err, response, content){
        if(err){
          res.render('errorPage', {error: {status: err.status, message: err.message}});
        }else{
            if(content.forecast.forecastday.length == 0){
                res.json(content.current.condition.icon);
                return;
            }
            var weather = {
                 longitude: req.query.long,
                 latitude: req.query.lat,
                 description: content.forecast.forecastday[0].day.condition.icon,
                 date: req.query.year+"-"+m+"-"+d+"T00:00:00.000Z"
            };
            
            var postWeatherParam = {
                url: paramsApi.server + paramsApi.apiURI + "/weather/",
                method: 'post',
                json: weather
            };
            
            request(postWeatherParam, function(errWP, responseWP, contentWP){
                if(errWP){
                    res.json({error: "There was an error"});
                }else{
                    res.json(contentWP.description);
                }
            });
            
        }
      });
  }
 
};