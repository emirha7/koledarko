var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.account = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}
		res.render('account', {
			title: 'Koledarko',
			cssFile: 'profileSettings',
			content: content
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/users/" + req.params.idOsebe,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

module.exports.editAccount = function (req, res) {

	if (req.body.nickname == null || req.body.nickname == "" || req.body.nickname.length > 20 ||
		req.body.oldpassword == null || req.body.oldpassword.length < 6 || req.body.oldpassword.length > 20 ||
		req.body.newpassword == null || req.body.newpassword.length < 6 || req.body.newpassword.length > 20 ||
		req.body.confrimpassword == null || req.body.confrimpassword != req.body.newpassword) {

		var error = {
			status: "400",
			message: "Invalid parameters in body"
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		if (req.body.oldpassword != content.password) {
			var error = {
				status: "400",
				message: "Invalid password"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		content.password = req.body.newpassword;

		var listRender2 = function (req, res, content) {
			if (content.errmsg != null || content.length == 0 || content.errors != null) {
				var error = {
					status: "400",
					message: "Bad request"
				};
				res.render('errorPage', {
					error: error
				});
				return;
			}

			if (content.status != null) {
				res.render('errorPage', {
					error: content
				});
				return;
			}
			res.redirect("/");
		}

		console.log(content.nickname);

		var reqParams2 = {
			url: paramsApi.server + paramsApi.apiURI + "/users/" + req.params.idOsebe,
			method: 'put',
			json: {
				username: content.username,
				password: content.password,
				nickname: req.body.nickname,
				email: content.email
			}
		};

		request(reqParams2, function (err, response, content) {
			if (err) {
				res.render('errorPage', {
					error: {
						status: err.status,
						message: err.message
					}
				});
			} else {
				listRender2(req, res, content);
			}
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/users/" + req.params.idOsebe,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};