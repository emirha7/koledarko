var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.events = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		res.render('sharedEvents', {
			title: 'Koledarko',
			cssFile: 'sharedEvents',
			content: content
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/shared",
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

module.exports.eventsPreview = function (req, res) {
	var eid = req.params.idEventa;

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		console.log(content.startTime);

		var startH = new Date(content.startTime).getHours();
		var endH = new Date(content.endTime).getHours();
		var startM = new Date(content.startTime).getMinutes();
		var endM = new Date(content.endTime).getMinutes();

		if (startH < 10)
			startH = "0" + startH;

		if (startM < 10)
			startM = "0" + startM;

		if (endH < 10)
			endH = "0" + endH;

		if (endM < 10)
			endM = "0" + endM;

		var vsebina = {
			title: content.title,
			location: content.location,
			startTime: startH + ":" + startM,
			endTime: endH + ":" + endM,
			note: content.note
		};

		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		var currentDate = new Date(content.startTime);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();

		var calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};

		var share = "Share";

		if (content.shared == "true") {
			share = "Unshare";
		}

		res.render('sharedEventsPreview', {
			title: 'Koledarko',
			cssFile: 'editEvent',
			content: vsebina,
			calendar: req.params.idKoledarja,
			event: req.params.idEventa,
			queryDate: currentYear + "-" + (currentMonth + 1) + "-" + currentDay,
			calendarInfo: calendarInfo,
			share: share
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + eid,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};