var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.getPage = function (req, res) {
    res.render('db', {});
}

module.exports.dropDb = function (req, res) {
   	var listRender = function (req, res, content) {
		console.log("CONTENT");
		console.log(content);
		if (content.errmsg != null || content.errors != null) {
			var error = {
				status: "500",
				message: "Something went wrong"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}
		
		if (content.status != null){
		   	res.render('errorPage', {
				error: content
			});
		}
		res.redirect("/");
	} 
	
	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/database",
		method: 'delete',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		console.log("ERROR");
		console.log(err);
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
}