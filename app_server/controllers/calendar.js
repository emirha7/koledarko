var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.shareEvent = function (req, res) {
	if (!checkReqBodyEvent(req.body) || req.body.date == null || req.body.date == "" || isNaN(new Date(req.body.date))) {
		var error = {
			status: "404",
			message: "Missing parameters in request body."
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}
		console.log("CONTENT");
		console.log(content);

		var listRender2 = function (req, res, content) {
			console.log("CONTENT2");
			console.log(content);
			if (content.errmsg != null || content.length == 0 || content.errors != null) {
				var error = {
					status: "400",
					message: "Bad request"
				};
				res.render('errorPage', {
					error: error
				});
				return;
			}

			if (content.status != null) {
				res.render('errorPage', {
					error: content
				});
				return;
			}

			res.redirect('/shared-events');
		}

		if (content.shared == "true") {
			content.shared = "false";
		} else {
			content.shared = "true";
		}
		
		console.log(content);

		var reqParams2 = {
			url: paramsApi.server + paramsApi.apiURI + "/events/" + req.params.idKoledarja + "/" + req.params.idEventa,
			method: 'put',
			json: content
		};

		request(reqParams2, function (err, response, content) {
			if (err) {
				res.render('errorPage', {
					error: {
						status: err.status,
						message: err.message
					}
				});
			} else {
				listRender2(req, res, content);
			}
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + req.params.idEventa,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

module.exports.calendar = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
		} else if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
		} else {
			var datum = new Date();

			if (req.query.q != null) {
				if (req.query.q == "" || isNaN(new Date(req.query.q))) {
					var error = {
						status: "400",
						message: "Missing date in URL."
					};
					res.render('errorPage', {
						error: error
					});
					return;
				}
				datum = new Date(req.query.q);
			}


			var events = [];
			var dateMonth = datum.getMonth() + 1;
			var dateDay = datum.getDate();
			var dateYear = datum.getFullYear();
			content.events = sortDates(content.events);
			for (var i = 0; i < content.events.length; i++) {
				var tempDatum = new Date(content.events[i].date);
				var tempMonth = tempDatum.getMonth() + 1;
				var tempDay = tempDatum.getDate();
				var tempYear = tempDatum.getFullYear();
				if (dateMonth == tempMonth && dateYear == tempYear) {
					var tempA = tempDatum.getHours();
					var tempB = tempDatum.getMinutes();

					if (tempA < 10)
						tempA = "0" + tempA;


					if (tempB < 10)
						tempB = "0" + tempB;

					var ev = {
						startTime: tempA + ":" + tempB,
						day: tempDay
					};
					events.push(ev);
				}
			}
			var appendData = constructEventData(events, datum);
			res.render('calendar', {
				contentD: content,
				cssFile: 'calendar',
				data: appendData,
				dataP: constructP(datum)
			});
		}
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.shareCalendar = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		var listRender2 = function (req, res, content) {
			if (content.errmsg != null || content.length == 0) {
				var error = {
					status: "400",
					message: "Bad request"
				};
				res.render('errorPage', {
					error: error
				});
				return;
			}
			console.log(content);
			res.redirect('/shared-calendars');
		}

		if (content.shared == "true") {
			content.shared = "false";
		} else {
			content.shared = "true";
		}

		var reqParams2 = {
			url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
			method: 'put',
			json: content
		};

		request(reqParams2, function (err, response, content) {
			if (err) {
				res.render('errorPage', {
					error: {
						status: err.status,
						message: err.message
					}
				});
			} else {
				listRender2(req, res, content);
			}
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.editCalendar = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		var info = {
			title: content.title,
			description: content.description
		};

		var share = "Share";

		if (content.shared == "true") {
			share = "Unshare";
		}

		res.render('editCalendar', {
			title: 'Koledarko',
			cssFile: 'editCalendar',
			calendar: req.params.idKoledarja,
			info: info,
			share: share
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.updateCalendar = function (req, res) {
	if (req.body.title == null || req.body.title == "" || req.body.description == null || req.body.description == "") {
		var error = {
			status: "404",
			message: "Missing parameters in request body."
		};
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}

		var listRender2 = function (req, res, content) {
			if (content.errmsg != null || content.length == 0 || content.errors != null) {
				var error = {
					status: "400",
					message: "Bad request"
				};
				res.render('errorPage', {
					error: error
				});
				return;
			}
			res.redirect('/calendar/' + req.params.idKoledarja);
		}

		content.title = req.body.title;
		content.description = req.body.description;

		var reqParams2 = {
			url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
			method: 'put',
			json: content
		};

		request(reqParams2, function (err, response, content) {
			if (err) {
				res.render('errorPage', {
					error: {
						status: err.status,
						message: err.message
					}
				});
			} else {
				listRender2(req, res, content);
			}
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

module.exports.addEvent = function (req, res) {
	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "404",
			message: "Missing date in URL."
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	var cid = req.params.idKoledarja;

	var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

	var currentDate = new Date(req.query.q);
	var currentDay = currentDate.getDate();
	var currentMonth = currentDate.getMonth();
	var currentYear = currentDate.getFullYear();

	var calendarInfo = {
		year: currentYear,
		month: mesci[currentMonth],
		day: currentDay,
		dayDisc: dnevi[currentDate.getDay()]
	};

	res.render('addEvent', {
		title: 'Koledarko',
		cssFile: 'addEvent',
		calendarInfo: calendarInfo,
		calendar: cid,
		date: req.query.q
	});
};


module.exports.addEventToBase = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}
		res.redirect('/calendar/' + req.params.idKoledarja + '/event?q=' + req.query.q);
	}

	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "404",
			message: "Missing date in URL."
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	if (!checkReqBodyEvent(req.body)) {
		var error = {
			status: "404",
			message: "Missing parameters in request body."
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	var startTime = concatTimeAndDate(req.body.startTime, req.query.q);
	var endTime = concatTimeAndDate(req.body.endTime, req.query.q);

	var json = {
		title: req.body.title,
		location: req.body.location,
		startTime: startTime,
		endTime: endTime,
		note: req.body.note,
		shared: "false"
	};

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + req.params.idKoledarja,
		method: 'post',
		json: json
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.editEvent = function (req, res) {
	var cid = req.params.idKoledarja;
	var eid = req.params.idEventa;

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		console.log(content.startTime);

		var startH = new Date(content.startTime).getHours();
		var endH = new Date(content.endTime).getHours();
		var startM = new Date(content.startTime).getMinutes();
		var endM = new Date(content.endTime).getMinutes();

		if (startH < 10)
			startH = "0" + startH;

		if (startM < 10)
			startM = "0" + startM;

		if (endH < 10)
			endH = "0" + endH;

		if (endM < 10)
			endM = "0" + endM;

		var vsebina = {
			title: content.title,
			location: content.location,
			startTime: startH + ":" + startM,
			endTime: endH + ":" + endM,
			note: content.note
		};

		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		var currentDate = new Date(content.startTime);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();

		var calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};

		var share = "Share";

		if (content.shared == "true") {
			share = "Unshare";
		}

		res.render('editEvent', {
			title: 'Koledarko',
			cssFile: 'editEvent',
			content: vsebina,
			calendar: req.params.idKoledarja,
			event: req.params.idEventa,
			queryDate: currentYear + "-" + (currentMonth + 1) + "-" + currentDay,
			calendarInfo: calendarInfo,
			share: share
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + eid,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.dayPreview = function (req, res) {
	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "400",
			message: "Missing date in URL."
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
		}

		if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
			return;
		}

		var events = sortDates(content.events);
		var currentDate = new Date(req.query.q);
		var currentDay = currentDate.getDate();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();

		var e = [];
		for (var i = 0; i < events.length; i++) {
			var eventDate = new Date(events[i].date);
			var eventDay = eventDate.getDate();
			var eventMonth = eventDate.getMonth();
			var eventYear = eventDate.getFullYear();

			if (eventDay == currentDay && eventMonth == currentMonth && eventYear == currentYear) {
				e.push(events[i]);
			}
		}

		var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		var calendarInfo = {
			year: currentYear,
			month: mesci[currentMonth],
			day: currentDay,
			dayDisc: dnevi[currentDate.getDay()]
		};

		res.render('dayPreview', {
			title: 'Koledarko',
			content: e,
			calendar: content._id,
			queryDate: req.query.q,
			cssFile: 'dayPreview',
			calendarInfo: calendarInfo
		});
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/" + req.params.idKoledarja,
		method: 'get',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.saveEditedEvent = function (req, res) {
	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};

			res.render('errorPage', {
				error: error
			});
			return;
		}
		res.redirect('/calendar/' + req.params.idKoledarja + '/event?q=' + req.query.q);
	}

	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "400",
			message: "Missing date in URL."
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}


	if (!checkReqBodyEvent(req.body)) {
		var error = {
			status: "404",
			message: "Missing parameters in request body."
		};

		res.render('errorPage', {
			error: error
		});
		return;
	}

	var startTime = concatTimeAndDate(req.body.startTime, req.query.q);
	var endTime = concatTimeAndDate(req.body.endTime, req.query.q);

	var json = {
		title: req.body.title,
		location: req.body.location,
		startTime: startTime,
		endTime: endTime,
		note: req.body.note
	};

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + req.params.idKoledarja + "/" + req.params.idEventa,
		method: 'put',
		json: json
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};


module.exports.removeEvent = function (req, res) {
	if (req.query.date == null || req.query.date == "" || isNaN(new Date(req.query.date)) || req.query.startTime == null || req.query.startTime == "" || !/^\d{2}:\d{2}$/.test(req.query.startTime || req.query.title == null || req.query.title == "")) {
		var error = {
			status: "404",
			message: "Missing or invalid parameters in URL"
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var currentDate = new Date(req.query.date);
	var currentDay = currentDate.getDate();
	var currentMonth = currentDate.getMonth();
	var currentYear = currentDate.getFullYear();

	var dnevi = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var mesci = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

	var calendarInfo = {
		year: currentYear,
		month: mesci[currentMonth],
		day: currentDay,
		dayDisc: dnevi[currentDate.getDay()]
	};

	res.render('removeEvent', {
		title: 'Koledarko',
		cssFile: 'removeEvent',
		calendarInfo: calendarInfo,
		calendar: req.params.idKoledarja,
		event: req.params.idEventa,
		eventInfo: {
			time: req.query.startTime,
			title: req.query.title
		},
		queryDate: req.query.date
	});
};


module.exports.removeEventFromBase = function (req, res) {
	if (req.query.q == null || req.query.q == "" || isNaN(new Date(req.query.q))) {
		var error = {
			status: "400",
			message: "Missing date parameter"
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
			return;
		}
		console.log(content);
		res.redirect('/calendar/' + req.params.idKoledarja + '/event?q=' + req.query.q);
	}

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/events/" + req.params.idKoledarja + "/" + req.params.idEventa,
		method: 'delete',
		json: {}
	};

	request(reqParams, function (err, response, content) {
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

function constructP(datum) {
	var x = datum;
	var year = x.getFullYear();
	var s = getStartDay(x.getFullYear(), x.getMonth() + 1);
	var z = datum.getMonth() + 1;
	x.setDate(0);

	return {
		m: z,
		y: year,
		last: x.getDate(),
		index: (s == 0) ? -1 : x.getDay()
	};
}

function constructEventData(events, datum) {
	var currentMY = getCurrentMonthYear(datum);
	var numberOfDays = daysInMonth(currentMY.m, currentMY.y);
	var hm = {};
	var s = getStartDay(currentMY.y, currentMY.m);

	for (var i = 1; i <= numberOfDays; i++) {
		hm[i] = {
			dday: i,
			events: []
		}
	}

	for (var i = 0; i < events.length; i++) {
		hm[events[i].day].events.push(events[i].startTime);
	}

	hm.startDay = s;
	hm.numberOfDays = numberOfDays;
	return hm;
}

function getStartDay(y, m) {
	var dateString = y + "-" + m + "-01";
	var d = new Date(dateString);
	return d.getDay();
}

function getCurrentMonthYear(datum) {
	return {
		m: datum.getMonth() + 1,
		y: datum.getFullYear()
	}
}

function daysInMonth(m, y) {
	return new Date(y, m, 0).getDate();
}

function sortDates(array) {
	return array.sort(function (a, b) {
		return new Date(a.date) - new Date(b.date);
	});
}

function concatTimeAndDate(time, date) {
	var dateTime = time;
	var dateQuery = date;

	var d = new Date(dateQuery);
	var temp = dateTime.split(":");

	d.setHours(temp[0]);
	d.setMinutes(temp[1]);

	return d;
}

function checkReqBodyEvent(body) {
	if (body.title == null || body.title == "" || body.location == null || body.location == "" || body.startTime == null || !/^\d{2}:\d{2}$/.test(body.startTime) ||
		body.endTime == null || !/^\d{2}:\d{2}$/.test(body.endTime) || body.note == null || body.note == "") {

		return false;
	}
	return true;
}