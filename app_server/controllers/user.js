var request = require('request');

var paramsApi = {
	server: 'http://localhost:' + process.env.PORT,
	apiURI: '/api'
};

if (process.env.NODE_ENV === 'production') {
	paramsApi.server = 'https://koledarkoapp.herokuapp.com';
}

module.exports.login = function (req, res) {
	res.render('login', {
		title: 'Login',
		cssFile: 'login'
	});
};

module.exports.loginToPage = function (req, res) {
	if (req.body.password == null || req.body.password == "" || req.body.username == null || req.body.username == "") {
		var error = {
			status: "400",
			message: "Bad request"
		};
		res.render('errorPage', {
			error: error
		});
		return;
	}

	var listRender = function (req, res, content) {
		console.log(content);
		if (content.errmsg != null || content.length == 0 || content.errors != null) {
			var error = {
				status: "400",
				message: "Bad request"
			};
			res.render('errorPage', {
				error: error
			});
		} else if (content.status != null) {
			res.render('errorPage', {
				error: content
			});
		} else {
			console.log(content);
			res.redirect('/calendar/' + content[0]._id);
		}
	}

	console.log("building params");

	var reqParams = {
		url: paramsApi.server + paramsApi.apiURI + "/calendars/login?username=" + req.body.username + "&password=" + req.body.password,
		method: 'get',
		json: {}
	};
	
	console.log(reqParams);

	request(reqParams, function (err, response, content) {
		console.log(err);
		
		if (err) {
			res.render('errorPage', {
				error: {
					status: err.status,
					message: err.message
				}
			});
		} else {
			listRender(req, res, content);
		}
	});
};

module.exports.register = function (req, res) {
	res.render('register', {
		title: 'Register',
		cssFile: 'signup'
	});
};

module.exports.createUser = function (req, res) {
	var listRender = function (req, res, content) {
		console.log(content);
		if (content.errmsg != null || content.errors != null) {
			var error = {
				status: "409",
				message: "Username is already taken."
			};
			res.render('errorPage', {
				error: error
			});
		} else {
			res.redirect('/');
		}
	}

	if (req.body.username == null || req.body.email == null || req.body.password == null || req.body.password_repeat == null || req.body.password != req.body.password_repeat ||
		req.body.username.length == 0 || req.body.email.length == 0 || req.body.password.length == 0 || req.body.password_repeat.length == 0 || req.body.email.indexOf('@') == -1 ||
		req.body.username.length > 20 || req.body.password.length < 6 || req.body.password.length > 20) {
		
		console.log("I AM HERE");
		
		var error = {
			status: "400",
			message: "Bad request"
		};
		res.render('errorPage', {
			error: error
		});
	} else {
		var reqParams = {
			url: paramsApi.server + paramsApi.apiURI + "/users/register",
			method: 'post',
			json: req.body
		};

		request(reqParams, function (err, response, content) {
			if (err) {
				res.render('errorPage', {
					error: {
						status: err.status,
						message: err.message
					}
				});
			} else {
				listRender(req, res, content);
			}
		});
	}
};