var express = require('express');
var router = express.Router();
var ctrlSharedEvents = require('../controllers/shared-events');

router.get('/:idEventa', ctrlSharedEvents.eventsPreview);
router.get('/', ctrlSharedEvents.events);

module.exports = router;