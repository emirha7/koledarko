var express = require('express');
var router = express.Router();
var ctrlAccount = require('../controllers/account');

router.get('/:idOsebe', ctrlAccount.account);
router.post('/:idOsebe', ctrlAccount.editAccount);

module.exports = router;