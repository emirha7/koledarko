var express = require('express');
var router = express.Router();
var ctrlDb = require('../controllers/db');

router.get('/', ctrlDb.getPage);
router.post('/', ctrlDb.dropDb);

module.exports = router;