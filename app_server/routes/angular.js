var express = require('express');
var router = express.Router();
var ctrAngular = require('../controllers/angular');

router.get('/', ctrAngular.angular);

module.exports = router;