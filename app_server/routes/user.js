var express = require('express');
var router = express.Router();
var ctrlUser = require('../controllers/user');

router.get('/', ctrlUser.login);
router.get('/login', ctrlUser.login);
router.post('/login', ctrlUser.loginToPage);
router.get('/register', ctrlUser.register);
router.post('/register', ctrlUser.createUser);

module.exports = router;