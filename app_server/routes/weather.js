var express = require('express');
var router = express.Router();
var ctrlWeather = require('../controllers/weather');

router.get('/', ctrlWeather.getWeather);

module.exports = router;