var express = require('express');
var router = express.Router();
var ctrlCalendar = require('../controllers/calendar');

router.get('/:idKoledarja', ctrlCalendar.calendar);
router.get('/:idKoledarja/edit', ctrlCalendar.editCalendar);
router.post('/:idKoledarja/edit', ctrlCalendar.updateCalendar);
router.post('/:idKoledarja/share', ctrlCalendar.shareCalendar);

router.get('/:idKoledarja/event', ctrlCalendar.dayPreview);
router.get('/:idKoledarja/event/add', ctrlCalendar.addEvent);
router.post('/:idKoledarja/event/add', ctrlCalendar.addEventToBase);

router.get('/:idKoledarja/event/:idEventa', ctrlCalendar.editEvent);
router.post('/:idKoledarja/event/:idEventa/share', ctrlCalendar.shareEvent);
router.post('/:idKoledarja/event/:idEventa', ctrlCalendar.saveEditedEvent);

router.post('/:idKoledarja/event/:idEventa/remove', ctrlCalendar.removeEventFromBase);
router.get('/:idKoledarja/event/:idEventa/remove', ctrlCalendar.removeEvent);

module.exports = router;