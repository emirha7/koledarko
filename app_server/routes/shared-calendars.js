var express = require('express');
var router = express.Router();
var ctrlSharedCalendar = require('../controllers/shared-calendars');

router.get('/:idKoledarja/event', ctrlSharedCalendar.previewCalendarEvents);
router.get('/:idKoledarja', ctrlSharedCalendar.previewCalendar);
router.get('/', ctrlSharedCalendar.calendar);

module.exports = router;