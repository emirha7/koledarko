var express = require('express');
var router = express.Router();
var ctrlSearch = require('../controllers/search');

router.get('/:idKoledarja/find', ctrlSearch.searchResults);
router.get('/:idKoledarja', ctrlSearch.search);

module.exports = router;